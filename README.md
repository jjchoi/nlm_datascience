﻿# **NLM DATA SCIENCE PROJECT**

### Data Science Methods for Candidate Compound Identification and Prioritization for Inclusion in the Hazardous Substance Data Bank (HSDB)

### *Folders:*

1. **Data** - input data files:
    - *candidates.csv* - candidate chemical data (gathered from non-HSDB hazardous chemical databases)
    - *candidates_negative.csv* - negative candidate chemical data (candidates identified by ICF as not suitable for HSDB inclusion)
    - *candidates_unclassified.csv* - unclassified candidate chemical data (remaining candidate chemicals for which priority scores will be generated)
    - *hsdb.csv* - HSDB chemical data (chemicals currently in HSDB)

2. **Chem-Prioritization-Scripts** - contains python scripts related to the candidate chemical prioritization stage:
    - *GSM.py* - Gower Similarity Model script
    - *SC.py* - Supervised Clustering script
    - *SML.py* - Supervised Machine Learning script
    - *SML_coefs.py* - Principal SML predictors identification script
    - *SML_ImputeOption.py* - SML script with impute option
    - *TSM.py* - Tanimoto Similarity Model script

3. **NLP-Pipeline** - contains python scripts and saved NLP models related to the NLP priority candidate chemical assessment stage:
    - *run_nlp_chemname.py* - script to run the pipeline with a chemical name
    - *run_nlp_cid.py* - script to run the pipeline with a CID
    - The *Models* and *Scripts* folders folders form the back-end of the pipeline

4. **Python-scripts** - contains python scripts and folders related to the Django web application.

### *Requirements:*
See **requirements.txt**
> **Note:** **TSM.py** requires an awkward installation of [rdkit](https://www.rdkit.org/docs/Install.html) that is not done via pip or conda install.

### *Running a chemical prioritization model*

Once you have the necessary dependencies installed, simply open the relevant model script in any python IDE and run it. You may
edit variables found at the bottom of the script, but that is not needed. When the run is complete, there should 
be an "**Output**" folder created in the working directory containing the output of the run.

### *Running the NLP pipeline*

Once you have the necessary dependencies installed, open the command line and navigate to **.\NLP-Pipeline**. Use the commands below to execute the scripts.
Double clicking a **.bat** file will execute a batch run. The **.bat** file can be edited in your favorite text editor.

- Running with chemical name
```
    python run_nlp_chemname.py "chloro(fluoro)methane"
``` 

- Running with CID
```
    python run_nlp_cid.py "11643"
```
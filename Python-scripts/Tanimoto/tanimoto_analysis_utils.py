from collections import defaultdict
import deprecation
import pandas as pd


def get_corresponding_hsdb_chemical(df):
    """
    Parameters:
    -----------
    df : dataframe : pandas dataframe
    -----------

    returns : List[str] : Corresponding chemical by the 'Max' column
    """
    out = []
    hsdb_chemicals_length = len(df['HSDB chemicals'])
    max_column_length = len(str(df['Max']))

    for i in range(hsdb_chemicals_length):
        if str(df['Max']) in df['HSDB chemicals'][i]:
            up_to = len(df['HSDB chemicals'][i]) - (max_column_length + 5)
            chemical = df['HSDB chemicals'][i][:up_to]

            if chemical not in out:
                out.append(chemical)

    return out


def to_dictionary(df):
    """
    Parameters:
    -----------
    df : dataframe : pandas dataframe
    -----------

    return : defaultdict : chemical to cid dictionary
    """
    d = defaultdict(list)
    for i in range(len(df)):
        d[df.iloc[i][0]].append(df.iloc[i][1])
        d[df.iloc[i][1]].append(df.iloc[i][0])
    return d


@deprecation.deprecated(details="Use the get_cid_values function instead")
def get_cids(df, col, dictionary):
    """
    Parameters:
    ----------
    df: dataframe : pandas dataframe
    col : str : column name
    dictionary : chemical to cid dictionary
    """
    column = []
    for row in df[col]:
        temp = []
        for chemical in row:
            if chemical in dictionary:
                temp.extend(dictionary[chemical])
        column.append(temp)
    return column


def get_cid_values(compounds, dictionary):
    """
    For a given compound, look up the corresponding cid information from the provided dictionary
    
    Parameters:
    ----------
    compounds : List[str] : List of compounds
    dictionary : Compound to cid dictionary
    ----------
    
    return : List[int] : List of cid values
    """
    cids = []
    for compound in compounds:
        if compound in dictionary:
            cids.extend(dictionary[compound])
    return cids


def candidate_smiles_filter(chemical):
    """
    Parameters:
    ----------
    col : str : chemical structure
    ----------
    
    returns boolean indicating whether the column value passes the filter or not
    """
    number_of_carbons = 0
    number_of_nitrogens = 0
    number_of_oxygens = 0
    chemical = chemical.upper()
    
    for chemical_char in chemical:
        if chemical_char == 'C':
            number_of_carbons += 1
        if chemical_char == 'N':
            number_of_nitrogens += 1
        if chemical_char == 'O':
            number_of_oxygens += 1
    
    if (number_of_carbons >= 4 and number_of_nitrogens >= 1) or (number_of_carbons >= 4 and number_of_oxygens >= 1):
        return True
    
    return False


def candidate_smiles_tanimoto_filter(score, threshold):
    """
    Parameters:
    ----------
    score : float : tanimoto score
    threshold : float : user defined threshold to filter data
    ----------
    
    returns boolean indicating whether the tanimoto score passes the threshold or not
    """
    return score < threshold


def find_top_k_values_within_list(nums, k):
    """
    For a list containing non negative floats, finds and returns the top 'k' elements within the list.
    
    Example:
    >>> print(find_top_5_values_within_list([4, 0, 4, 0, 3, 2], 3))
    >>> [4, 3, 4]
    
    Parameters:
    ----------
    nums: List[float] : List with float numbers
    k: int : number of elements to return
    ----------
    
    return: List[float] : Top "k" elements
    """
    n = len(nums)
    if n < k:
        raise Exception("k needs to be smaller than the length of the list")
    
    out = [-1 for i in range(k)]
    for num in nums:
        min_value = min(out)
        min_value_index = out.index(min_value)
        
        if num > min_value:
            out[min_value_index] = num
    return out
    

def find_corresponding_indices(main_nums, nums):
    """
    Given a list of float values, find the corresponding indices from a different list.
    
    Example:
    >>> x = [4.0, 0.0, 4.0, 0.0, 3.0, 2.0]
    >>> print(find_corresponding_indices(x, [4, 3, 4]))
    >>> [0, 2, 4]
    
    Parameters:
    ----------
    nums : List[float] : List of float values
    ----------
    
    return : List[int] : List of corresponding indices
    """
    nums = set(nums) # remove duplicate values -- for loop will add all the indices in one pass
    s = set()

    try:
        for i in range(len(main_nums)):
            if main_nums[i] in nums:    
                s.add(i)
    except:
        pass
        
    return list(s)


def get_column_names_given_indices(indices, column_names):
    """
    Given a list of indices, find the corresponding column names for each rows within a pandas dataframe.
    
    Example:
    >>> df = pd.DataFrame({'a': [4], 'b': [0], 'c': [4], 'd': [0], 'e': [3], 'f': [2]})
    >>> print(df)
    
    >>>   a b c d e f
        0 4 0 4 0 3 2
        
    >>> column_names = df.columns
    
    >>> print(get_column_names_given_indices([0, 2, 4], column_names))
    >>> ['a', 'c', 'e']
    
    Parameters:
    ----------
    indices : List[int] : List of integer indices
    column_names : List[str] : List of corresponding column names
    ----------
    
    return : List[str] : List of corresponding column names
    """
    out = []
    for index in indices:
        out.append(column_names[index])
    return out


def get_column_values_given_indices(indices, values):
    """
    Given a list of indices, retrieve the associated values
    
    Example:
    >>> indices = [4, 5]
    >>> values = [4, 0, 4, 0, 3, 2]
    
    >>> print(get_column_values_given_indices(indices, values))
    >>> [3, 2]
    
    Parameters:
    ----------
    indices : List[int] : List of column indices
    values : List[T] : List of values
    ----------
    
    result : List[T] : List of corresponding values
    """
    out = []
    for index in indices:
        out.append(values[index])
    return out


def get_column_values_given_column_names(column_names, values):
    """
    Given a list of column_names, retrieve the associated values.
    
    Example:
    >>> df = pd.DataFrame({'a': [4], 'b': [0], 'c': [4], 'd': [0], 'e': [3], 'f': [2]})
    >>> print(df)
    
    >>>   a b c d e f
        0 4 0 4 0 3 2
    
    >>> print(get_column_names_given_indices(['a', 'c', 'e'], [4, 0, 4, 0, 3, 2]))
    >>> [4, 4, 3]
    
    Parameters:
    ----------
    indices : List[int] : List of column indices
    values : List[T] : List of values
    ----------
    
    result : List[T] : List of corresponding values
    """
    out = []
    for column_name in column_names:
        out.append(values[column_name])
    return out


def remove_value_from_list(list_values, value):
    """
    Removes a specific value from a list of values.
    
    Example:
    >>> print(remove_value_from_list(['a', 'b', 'c'], 'b'))
    >>> ['a', 'c']
    
    Parameters:
    ----------
    list_values : List[T] : List of values
    value : str : Specified value
    ----------
    
    return : List[str] : List of column names without the specified value
    """
    out = []
    for val in list_values:
        if val != value:
            out.append(val)
    return out
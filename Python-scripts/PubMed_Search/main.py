import sys
import pandas as pd
import time
from combine_abstract_information import CombineAbstractInformation


def main():
    """
    Main function to pull titles and abstracts based on query. This will create a csv file to store outputs.
    """

    """
    positive_file_path = "~/Documents/NLM3/data/pub_med/"
    negative_file_path = "~/Documents/NLM3/data/pub_med/"

    df_positive = pd.read_csv(positive_file_path + "positive.csv")
    df_negative = pd.read_csv(negative_file_path + "negative.csv")

    rmax = 10 # max returned records (as string)
    eml = sys.argv[1] # user input email

    positive_data = pd.DataFrame()
    negative_data = pd.DataFrame()

    for i in range(len(df_positive)):
        time.sleep(1)
        print(f"Processing {i+1} out of {len(df_positive)}")
        query = df_positive.loc[i, "cids"]
        print(query)
        details = CombineAbstractInformation(query, rmax, eml)
        details_combined = details.combine_details()
        positive_data = positive_data.append(pd.DataFrame(details_combined).T)

    for i in range(len(df_negative)):
        time.sleep(1)
        print(f"Processing {i+1} out of {len(df_negative)}")
        query = df_negative.loc[i, "cids"]
        details = CombineAbstractInformation(query, rmax, eml)
        details_combined = details.combine_details()
        negative_data = negative_data.append(pd.DataFrame(details_combined).T)

    positive_data = positive_data.reset_index(drop=True)
    negative_data = negative_data.reset_index(drop=True)

    columns = {0: 'cids', 1: 'title', 2: 'abstract', 3: 'publication_year'}
    positive_data = positive_data.rename(columns=columns)
    negative_data = negative_data.rename(columns=columns)

    positive_data.to_csv(positive_file_path + "positive_output.csv", index=False)
    negative_data.to_csv(negative_file_path + "negative_output.csv", index=False)
    """

    file_path = "~/Documents/NLM3/data/django_demo_data/output/interested_compounds_original_where_focus_is_0.csv"

    df = pd.read_csv(file_path)

    rmax = 10 # max returned records (as string)
    eml = sys.argv[1] # user input email

    data = pd.DataFrame()

    for i in range(len(df)):
        time.sleep(1)
        print(f"Processing {i+1} out of {len(df)}")
        query = df.loc[i, "PMID"]
        print(query)
        details = CombineAbstractInformation(query, rmax, eml)
        details_combined = details.combine_details()
        data = data.append(pd.DataFrame(details_combined).T)

    data = data.reset_index(drop=True)

    columns = {0: 'pmids', 1: 'title', 2: 'abstract', 3: 'publication_year'}
    data = data.rename(columns=columns)

    output_file_path = "~/Documents/NLM3/data/django_demo_data/output/interested_compounds_original_where_focus_is_0_output.csv"

    data.to_csv(output_file_path, index=False)

if __name__ == '__main__':
    main()

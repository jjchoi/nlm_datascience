from abstract_details_search import AbstractDetailsSearch


class CombineAbstractInformation(AbstractDetailsSearch):
    """
    Get and combine details: pmids, titles, abstracts, and publication year

    Attributes:
    -----------
    query : str : The query
    rmax : str : Max returned records (as string)
    eml : str : User email id
    """
    def __init__(self, query, rmax, eml):
        super().__init__(query, rmax, eml)


    def combine_details(self):
        pmids = []
        titles = []
        abstracts = []
        publication_years = []

        for pubmed_article in self.pubmed_articles:
            pmids.append(self.get_pmid(pubmed_article))
            titles.append(self.get_title(pubmed_article))
            abstracts.append(self.get_abstract(pubmed_article))
            publication_years.append(self.get_publication_year(pubmed_article))

        return [pmids, titles, abstracts, publication_years]

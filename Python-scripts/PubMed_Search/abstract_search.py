from Bio import Entrez
from pandas import DataFrame
from os import getcwd


class AbstractSearch:
    """
    Pulls titles and abstracts based on the provided query from PUBMED

    Attributes:
    -----------
    query : str : The query
    rmax : str : Max returned records (as string)
    eml : str : User email id
    """
    def __init__(self, query, rmax, eml):
        """
        Initializes AbstractSearch class with the provided attributes
        """
        self.query = query
        self.rmax = rmax

        # Assigns email id to search
        Entrez.email = eml


    def get_pmids(self):
        """
        Pulls PMIDs by a search term

        Parameters:
        -----------
        query : str : Search term query
        -----------

        return : List[str] : A string list of PMIDs
        """
        handle = Entrez.esearch(db='pubmed',
                                sort='relevance',
                                retmax=self.rmax,
                                retmode='xml',
                                term=self.query)
        results = Entrez.read(handle)
        handle.close()
        return results['IdList']


    def get_abstract_details(self, id_list):
        """
        Returns abstract details by its PMID(s)

        Parameters:
        -----------
        id_list : List[str] : A list of PMID(s)
        -----------

        return : dict[T,T] : Corresponding abstract details
        """
        if not id_list:
            return {}

        ids = ','.join(id_list)
        handle = Entrez.efetch(db='pubmed',
                               retmode='xml',
                               id=ids)
        results = Entrez.read(handle)
        handle.close()

        return results

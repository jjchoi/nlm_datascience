from abstract_search import AbstractSearch


class AbstractDetailsSearch(AbstractSearch):
    """
    Initializes the abstract details using the provided query.

    Attributes:
    -----------
    query : str : The query
    rmax : str : Max returned records (as string)
    eml : str : User email id
    """
    def __init__(self, query, rmax, eml):
        """
        Inherits the AbstractSearch class and instantiates abstract details
        """
        super().__init__(query, rmax, eml)
        self.pmids = self.get_pmids()
        try:
            self.pubmed_articles = self.get_abstract_details(self.pmids)['PubmedArticle']
        except:
            self.pubmed_articles = []


    def get_title(self, pubmed_article):
        """
        Gets abstract title from details

        Parameters:
        ----------
        pubmed_article : List[dict[T,T]] : dictionary object that hold pubmed article details
        ----------

        return : str : pubmed article title
        """
        title_text = ''

        try:
            title_text = pubmed_article[u'MedlineCitation'][u'Article'][u'ArticleTitle']
            if isinstance(title_text, bytes):
                title_text = title_text.encode('ascii', 'ignore')
            else:
                title_text = str(title_text)
        except:
            print('Title is unavailable. Returning empty string instead.')

        return title_text


    def get_pmid(self, pubmed_article):
        """
        Gets pmid (cid) from details

        Parameters:
        ----------
        pubmed_article : List[dict[T,T]] : dictionary object that hold pubmed article details
        ----------

        return : str : pubmed article pmid (cid)
        """
        pmid = ''

        try:
            pmid = str(pubmed_article['MedlineCitation']['PMID'])
        except:
            print('PMID is unavailable. Returning empty string instead')

        return pmid


    def get_abstract(self, pubmed_article):
        """
        Gets abstract text from details

        Parameters:
        ----------
        pubmed_article : List[dict[T,T]] : dictionary object that hold pubmed article details
        ----------

        return : str : pubmed article abstract text
        """
        abstract_text = ''

        try:
            abstract_text = pubmed_article[u'MedlineCitation'][u'Article'][u'Abstract'][u'AbstractText'][0]
            if isinstance(abstract_text, bytes):
                abstract_text = abstract_text.encode('ascii', 'ignore')
            else:
                abstract_text = str(abstract_text)
        except:
            print('Abstract is unavailable. Returning empty string instead')

        return abstract_text


    def get_publication_year(self, pubmed_article):
        """
        Gets publication year from details

        Parameters:
        ----------
        pubmed_article : List[dict[T,T]] : dictionary object that hold pubmed article details
        ----------

        return : str : pubmed publication year
        """
        publication_year = ''

        try:
            publication_year = pubmed_article['MedlineCitation']['Article']['Journal']['JournalIssue']['PubDate']['Year']
        except:
            print('Publication year is unavailable. Returning empty string instead')

        return publication_year

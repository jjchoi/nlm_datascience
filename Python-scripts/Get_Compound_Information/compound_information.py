import pubchempy as pcp
import sys
import re
import time

class CompoundInformation:
    """
    Gets various compound information such as CIDs and CASRNs

    Attributes:
    -----------
    search_query : str :  The compound identifier to use as a search query
    """
    def __init__(self, search_query):
        try:
            self.compounds = pcp.get_compounds(search_query, 'name')

        except ValueError as e:
            print('=========================================================')
            print('= Error while decoding response contents. Trying again. =')
            print('=========================================================')

            try:
                self.compounds = pcp.get_compounds(search_query, 'name')
            except ValueError as e:
                print(e)
                self.compounds = ''
        # TODO: define specific error
        except:
            print('Connection not stable - Waiting 5 seconds before reconnecting')
            self.compounds = ''
            time.sleep(5)


    def get_cid(self):
        """
        For a given search query, get a list of CIDs

        return : List[str] : Returns a list of CIDs if it exists. Otherwise, it
                             will return an empty list.
        """
        #time.sleep(0.5)
        try:
            return [compound.cid for compound in self.compounds]
        except:
            print('CID not found -- returning empty list.')
            return []



    def get_casrn(self):
        """
        For a given search query, get a list of CAS Registry Numbers

        return : List[str] : A list of CAS registry numbers if it exists.
        """
        #time.sleep(0.5)
        casrn_patterns = ['^CAS', '^[0-9]{1,6}-[0-9]{1,3}-[0-9]{1}$']
        casrns = []
        # TODO: Improve time complexity
        try:
            for compound in self.compounds:
                for synonym in compound.synonyms:
                    for casrn_pattern in casrn_patterns:
                        if re.search(casrn_pattern, synonym):
                            casrns.append(synonym)
            return casrns
        except:
            print('CASRN not found -- returning empty list.')
            return casrns

from compound_information import CompoundInformation

class CompoundCASRNSearch:
    """
    Gets compound cas registration number

    Attributes:
    -----------
    df : pandas DataFrame : Dataframe containing search criterias
    saved_result : dict : Dictionary of query search results
    """
    def __init__(self, df):
        self.df = df
        self.saved_result = {}


    def get_casrns_using_hsdb_docno(self):
        """
        Gets CASRN(s) by combining two fields from the dataframe: "Parent_table" and "DOCNO"

        return : List[List[str]] : Returns a list of CASRN(s) if it exists. Otherwise, it will return an empty list.
        """
        casrns = []
        counter = 1
        for hsdb_docno in self.df['Parent_table'] + ' ' + self.df['DOCNO'].astype(str):
            print("({}/{}) Getting casrn value(s) for the search query : {}".format(counter, len(self.df), hsdb_docno))
            counter += 1
            self._get_all_casrns(casrns, hsdb_docno)
        return casrns


    def get_casrns_using_casrn(self):
        """
        Get additional CASRN(s) by using the "CASRegistryNumber" field from the dataframe.

        return : List[List[str]] : Returns a list of CASRN(s) if it exists. Otherwise, it will return an empty list.
        """
        casrns = []
        counter = 1
        for cas_registry_number in self.df['CASRegistryNumber']:
            print("({}/{}) Getting casrn value(s) for the search query : {}".format(counter, len(self.df), cas_registry_number))
            counter += 1
            self._get_all_casrns(casrns, cas_registry_number)
        return casrns


    def get_casrns_using_substance_name(self):
        """
        Get CASRN(s) by using the "NameOfSubstance" field from the dataframe

        return : List[List[str]] : Returns a list of CASRN(s) if it exists. Otherwise, it will return an empty list.
        """
        casrns = []
        counter = 1
        for name_of_substance in self.df['NameOfSubstance']:
            print("({}/{}) Getting casrn value(s) for the search query : {}".format(counter, len(self.df), name_of_substance))
            counter += 1
            self._get_all_casrns(casrns, name_of_substance)
        return casrns


    def get_casrns_using_synonym(self):
        """
        Get CASRN(s) by using the "Synonym" field from the dataframe

        return : List[List[str]] : Returns a list of CASRN(s) if it exists. Otherwise, it will return an empty list.
        """
        casrns = []
        counter = 1
        for synonym in self.df['Synonym']:
            print("({}/{}) Getting casrn value(s) for the search query : {}".format(counter, len(self.df), synonym))
            counter += 1
            self._get_all_casrns(casrns, synonym)
        return casrns


    def _get_all_casrns(self, casrns, search_criteria):
        """
        Get all CASRN(s) subject to its search_criteria

        Parameters:
        -----------
        casrn : List[str] : Empty list
        search_criteria : str : Search query to get casrns from compound information
        -----------

        returns : List[str] : List of returned casrns from compound information
        """
        if not search_criteria:
            casrns.append([])
        elif search_criteria in self.saved_result:
            casrns.append(self.saved_result[search_criteria])
        else:
            compound_info = CompoundInformation(search_criteria)
            temp_casrns = compound_info.get_casrn()
            print(temp_casrns)
            casrns.append(temp_casrns)
            self.saved_result[search_criteria] = temp_casrns
        return casrns

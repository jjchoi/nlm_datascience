from compound_information import CompoundInformation

class CompoundCIDSearch:
    """
    Gets compound cid information

    Attributes:
    -----------
    df : pandas DataFrame : Dataframe containing search criterias
    """
    def __init__(self, df):
        self.df = df
        self.saved_result = {}


    def get_cids_using_hsdb_docno(self):
        """
        Gets CID by combining two fields from the dataframe: "Parent_table" and "DOCNO"

        return : List[List[int]] : Returns a list of CIDs if it exists. Otherwise, it will return an empty list.
        """
        cids = []
        counter = 1
        for hsdb_docno in self.df['Parent_table'] + ' ' + self.df['DOCNO'].astype(str):
            print("({}/{}) Getting cid value(s) for the hsdb + docno search query : {}".format(counter, len(self.df), hsdb_docno))
            counter += 1
            self._get_all_cids(cids, hsdb_docno)
        return cids


    def get_cids_using_casrn(self):
        """
        Get CIDs using the "CASRegistryNumber" field from the dataframe

        return : List[List[int]] : Returns a list of CIDs if it exists. Otherwise, it will return an empty list.
        """
        cids = []
        counter = 1
        for cas_registry_number in self.df['CASRegistryNumber'].astype(str):
            print("({}/{}) Getting cid value(s) for the casrn search query : {}".format(counter, len(self.df), cas_registry_number))
            counter += 1
            self._get_all_cids(cids, cas_registry_number)
        return cids


    def get_cids_using_substance_name(self):
        """
        Get CIDs using the "NameOfSubstance" field from the dataframe

        return : List[List[int]] : Returns a list of CIDs if it exists. Otherwise, it will return an empty list.
        """
        cids = []
        counter = 1
        for name_of_substance in self.df['NameOfSubstance']:
            print("({}/{}) Getting cid value(s) for the substance name search query : {}".format(counter, len(self.df), name_of_substance))
            counter += 1
            self._get_all_cids(cids, name_of_substance)
        return cids


    def get_cids_using_synonym(self):
        """
        Get CIDs using the "Synonym" field from the dataframe

        return : List[List[int]] : Returns a list of CIDs if it exists. Otherwise, it will return an empty list.
        """
        cids = []
        counter = 1
        for synonym in self.df['Synonym']:
            print("({}/{}) Getting cid value(s) for the synonym search query : {}".format(counter, len(self.df), "r'" + synonym))
            counter += 1
            self._get_all_cids(cids, synonym)
        return cids


    def _get_all_cids(self, cids, search_criteria):
        """
        Get all CIDS(s) subject to its search_criteria

        Parameters:
        -----------
        cids : List[] : Empty list
        search_criteria : str : Search query to get cids from compound information
        -----------

        returns : List[str] : List of returned cids from compound information
        """
        if search_criteria is None:
            return []
        elif search_criteria in self.saved_result:
            cids.append(self.saved_result[search_criteria])
        else:
            compound_info = CompoundInformation(search_criteria)
            temp_cid = compound_info.get_cid()
            print(temp_cid)
            cids.append(temp_cid)
            self.saved_result[search_criteria] = temp_cid
        return cids

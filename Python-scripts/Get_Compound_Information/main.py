from compound_information import CompoundInformation
from compound_cid_search import CompoundCIDSearch
from compound_casrn_search import CompoundCASRNSearch
from os import getcwd
import pandas as pd
import http.client as http
import time
import sys

def main():
    """
    Main function to pull cid(s) and casrn(s) from pubchem.

    Examples:
    > python main.py "test"
    """

    # Define file name
    FILENAME = sys.argv[1] # file name

    # Handling IncompleteRead error
    # https://stackoverflow.com/questions/14442222/how-to-handle-incompleteread-in-python
    http.HTTPConnection._http_vsn = 10
    http.HTTPConnection._http_vsn_str = 'HTTP/1.0'

    # Read file
    df = pd.read_csv("~/Documents/NLM3/data/hsdb_missing_cid/QA_hsdb_missing_cid.csv", dtype=object)
    df = df.fillna('')

    # Test with subset
    df = df.iloc[:100]

    # Instantiate
    compound_cid_search = CompoundCIDSearch(df)
    compound_casrn_search = CompoundCASRNSearch(df)

    # Get cid information
    df['hsdb_docno_cids'] = compound_cid_search.get_cids_using_hsdb_docno()
    df['casrn_cids'] = compound_cid_search.get_cids_using_casrn()
    df['substance_name_cids'] = compound_cid_search.get_cids_using_substance_name()
    df['synonym_cids'] = compound_cid_search.get_cids_using_synonym()

    # Get casrn information
    df['hsdb_docno_casrns'] = compound_casrn_search.get_casrns_using_hsdb_docno()
    df['casrn_casrns'] = compound_casrn_search.get_casrns_using_casrn()
    df['substance_name_casrns'] = compound_casrn_search.get_casrns_using_substance_name()
    df['synonym_casrns'] = compound_casrn_search.get_casrns_using_synonym()

    # Write to file
    df.to_csv(getcwd() + r'\\' + FILENAME + '.csv', index=False)


if __name__ == '__main__':
    main()

import pandas as pd
import re


def join_title_and_abstract(title, abstract):
    """
    Joins title and abstract

    Parameters:
    -----------
    title: str: string format title
    abstract: str: string format abstract
    -----------
    return: str: string of title and abstract
    """
    return str(title) + ' '  + str(abstract)


def keep_only_alpha_numeric(text):
    """
    Keep only alpha numeric values

    Parameters:
    ----------
    text : str : string value text
    ----------

    return : str : string value text
    """
    text = re.sub("'", "", str(text))
    text = re.sub("(\\W)+", " ", text)
    return text

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

import torch.nn.functional as F
import torch
import os
import pandas as pd

from pub_med_search.combine_abstract_information import CombineAbstractInformation
from pub_med_search.utils import (join_title_and_abstract, keep_only_alpha_numeric)

from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.http import HttpResponseRedirect
from django.contrib import messages

from bert_model.forms import AbstractForm
from django import forms
from transformers import BertForTokenClassification, BertTokenizer, BertForSequenceClassification
from torch.utils.data import TensorDataset, DataLoader, RandomSampler, SequentialSampler
from keras.preprocessing.sequence import pad_sequences


def home(request):
    # Go to nlp model home page
    return render(request, 'bert_model/bert_model_home.html')


def jump_to_output(request, cid):
    # For demonstration purposes, read it from a file -- later move it to the database
    path = os.getcwd() + "/bert_model/static/bert_model/"
    df = pd.read_csv(path + 'biobert_model_outputs.csv', dtype={"Publication_Year": "str"})
    df = df[df.CIDs == cid].sort_values(by='Probability', ascending=False)
    html_table = df[['CIDs', 'PMIDs', 'Title', 'Abstract', 'Publication_Year', 'Probability', 'Label', ]].to_html(classes=["table-bordered", "table-hover"], index=False)
    content = {'html_table': html_table}
    return render(request, 'bert_model/bert_model_outputs.html', content)


# Use to run everything live
class RankingTitlesAndAbstracts(TemplateView):
    template_name1 = 'bert_model/bert_model_home.html'
    template_name2 = 'bert_model/bert_model_outputs.html'
    MAX_LEN = 200
    DEVICE = torch.device("cpu")

    # With any request that comes in, we are simply creating a blank form and rendering
    # it back out to the template
    def get(self, request):
        form = AbstractForm()
        return render(request, self.template_name1, {'form': form})


    def post(self, request):
        df = pd.DataFrame()
        output = []

        form = AbstractForm(request.POST)
        if form.is_valid():
            cid = form.cleaned_data['cid']
            maximum_number_of_results_to_return = form.cleaned_data['maximum_number_of_results_to_return']
            email = form.cleaned_data['email']

            df = self._get_query_details_as_df(df, cid, maximum_number_of_results_to_return, email)
            if df.empty:
                messages.error(request, 'The provided CID does not exist. Please enter in a different value.')
                return redirect('bert_model_home')

            df = self._process_df(df)

            output = self._classification(df, output)
            html_table = df[['cid', 'title', 'abstract', 'publication_year']].to_html(classes=["table-bordered"], index=False)

            args = {'form': form, 'output': output, 'html_table': html_table}
        else:
            args = {'form': form}
        return render(request, self.template_name2, args)


    def _get_query_details_as_df(self, df, query, rmax, email):
        details = CombineAbstractInformation(query, rmax, email)
        details_combined = details.combine_details()
        df = df.append(pd.DataFrame(details_combined).T)
        columns = {0: 'cid', 1: 'title', 2: 'abstract', 3: 'publication_year'}
        df = df.rename(columns=columns)
        return df


    def _process_df(self, df):
        df = df.fillna('') # Replace all NAN values with ''
        df['title_abstract'] = df.apply(lambda x: join_title_and_abstract(x['title'], x['abstract']), axis=1) # Combine the two fields
        df['title_abstract'] = df.apply(lambda x: keep_only_alpha_numeric(x['title_abstract']), axis=1) # Keep only alpha numeric values
        return df


    def _classification(self, df, output):
        path = os.getcwd() + "/bert_model/static/bert_model/model_save/"

        model = BertForSequenceClassification.from_pretrained(path, num_labels=2)
        tokenizer = BertTokenizer.from_pretrained(path)

        sentences = df['title_abstract'].values

        input_ids = []
        attention_masks = []

        for sent in sentences:
            encoded_sent = tokenizer.encode(sent, add_special_tokens=True)
            input_ids.append(encoded_sent)

        input_ids = pad_sequences(input_ids, maxlen=self.MAX_LEN, dtype='long', truncating='post', padding='post')

        for seq in input_ids:
            seq_mask_test = [float(i > 0) for i in seq]
            attention_masks.append(seq_mask_test)

        prediction_inputs = torch.tensor(input_ids)
        prediction_masks = torch.tensor(attention_masks)

        prediction_data = TensorDataset(prediction_inputs, prediction_masks)
        prediction_sampler = SequentialSampler(prediction_data)
        prediction_dataloader = DataLoader(prediction_data, sampler=prediction_sampler, batch_size=1)

        model.eval()

        for batch in prediction_dataloader:
            b = tuple(t.to(self.DEVICE) for t in batch)
            b_input_ids, b_input_mask = b

            with torch.no_grad():
                outputs = model(b_input_ids.to(self.DEVICE).long(), token_type_ids=None, attention_mask=b_input_mask.to(self.DEVICE))

            logits = outputs[0]
            softmax = F.softmax(logits, dim=1)

            logits = logits.detach().cpu().numpy()

            output.append(softmax)

        return output

from django import forms
import re


class AbstractText(forms.Form):
    abstract = forms.CharField(widget=forms.Textarea)


class AbstractForm(forms.Form):
    cid = forms.CharField(widget=forms.TextInput(attrs={'style':'max-width: 10em'}))
    maximum_number_of_results_to_return = forms.CharField(widget=forms.TextInput(attrs={'style':'max-width: 5em'}))
    email = forms.CharField(widget=forms.TextInput(attrs={'style':'max-width: 20em'}))

    def clean(self):
        clean_data = super().clean()

        cid_value = clean_data.get('cid')
        max_num_row_value = clean_data.get('maximum_number_of_results_to_return')
        email_value = clean_data.get('email')

        cid_invalid = not re.search('^[0-9]*$', cid_value)
        row_invalid = not re.search('^[0-9]*$', max_num_row_value)
        email_invalid = not re.match(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$", email_value)

        # cid
        if cid_invalid and not row_invalid and not email_invalid:
            raise forms.ValidationError({
                'cid': "Please enter a valid value. This field should contain only numeric values",
            })

        # row
        elif not cid_invalid and row_invalid and not email_invalid:
            raise forms.ValidationError({
                'maximum_number_of_results_to_return': "Please enter a valid value. This field should contain only numeric values",
            })

        # email
        elif not cid_invalid and not row_invalid and email_invalid:
            raise forms.ValidationError({
                'email': "Please enter a valid email."
            })

        # cid, row
        elif cid_invalid and row_invalid and not email_invalid:
            raise forms.ValidationError({
                'cid': "Please enter a valid value. This field should contain only numeric values",
                'maximum_number_of_results_to_return': "Please enter a valid value. This field should contain only numeric values",
            })

        # cid, email
        elif cid_invalid and not row_invalid and email_invalid:
            raise forms.ValidationError({
                'cid': "Please enter a valid value. This field should contain only numeric values",
                'email': "Please enter a valid email."
            })

        # row, email
        elif not cid_invalid and row_invalid and email_invalid:
            raise forms.ValidationError({
                'maximum_number_of_results_to_return': "Please enter a valid value. This field should contain only numeric values",
                'email': "Please enter a valid email."
            })

        # cid, row, and email
        elif cid_invalid and row_invalid and email_invalid:
            raise forms.ValidationError({
                'cid': "Please enter a valid value. This field should contain only numeric values",
                'maximum_number_of_results_to_return': "Please enter a valid value. This field should contain only numeric values",
                'email': "Please enter a valid email."
            })

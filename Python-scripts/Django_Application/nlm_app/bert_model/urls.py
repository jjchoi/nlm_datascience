from django.urls import path
from . import views


urlpatterns = [
    path('', views.RankingTitlesAndAbstracts.as_view(), name='bert_model_home'),
    path('<int:cid>/', views.jump_to_output, name='jump_to_output'),
]

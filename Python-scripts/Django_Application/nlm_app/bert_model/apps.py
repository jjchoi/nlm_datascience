from django.apps import AppConfig


class BertModelConfig(AppConfig):
    name = 'bert_model'

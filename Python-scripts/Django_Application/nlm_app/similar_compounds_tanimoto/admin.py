from django.contrib import admin
from .models import Tanimoto, TanimotoUpdated
from import_export.admin import ImportExportModelAdmin

# Register your models here.
@admin.register(Tanimoto)
class ViewAdmin(ImportExportModelAdmin):
    pass


@admin.register(TanimotoUpdated)
class ViewAdmin(ImportExportModelAdmin):
    pass

from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect, HttpRequest
from .models import TanimotoUpdated
from ast import literal_eval
from similar_compounds_tanimoto.forms import JumpToRowForm
from django.views.generic import TemplateView
from django.contrib import messages
import re


def home(request):
    # Go to home page
    return render(request, 'similar_compounds_tanimoto/home.html')


class CompareChemicals(TemplateView):
    template_name = 'similar_compounds_tanimoto/compare_chemicals.html'

    # With any request that comes in, we are simply creating a blank form and rendering
    # it back out to the template
    def get(self, request):
        form = JumpToRowForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = JumpToRowForm(request.POST)
        if form.is_valid():
            text = form.cleaned_data['row_number']
            return HttpResponseRedirect('row_num=' + text)
        else:
            messages.error(request, form.errors.get_json_data()['__all__'][0]['message'])
        return redirect('compare_chemicals')


def compare_chemicals_iterate_rows(request, row_num):
    """ Initiates the TanimotoUpdated object and retrieves the next or previous row's
    'row_candidate_cids' and 'row_max_cids'. It takes the first values from each of
    the cids, appends those values to the pubchem url path, and is then fed into
    the row object that is then used by compare_chemicals_iterate_rows.html to
    display the information

    Parameters:
    -----------
    row_num: int : row number
    -----------

    returns a 'row' dictionary
    """
    length_tanimoto = len(TanimotoUpdated.objects.all())

    pubchem_path = 'https://pubchem.ncbi.nlm.nih.gov/compound/'
    row = get_object_or_404(TanimotoUpdated, pk=row_num)
    row_candidate_cids = row.candidate_cids
    row_max_cids = row.max_cids
    row.total_number_of_rows = length_tanimoto
    row.tanimoto_percent = str(round(float(row.max) * 100, 2)) + '%'

    # row_candidate_cids and row_max_cids are string values (ie "[123, 456]").
    # However, we want a list value (ie List[str]) - and hence we change them.
    literal_row_candidate_cids = literal_eval(row_candidate_cids)
    literal_row_max_cids = literal_eval(row_max_cids)

    # Get length of the lists -- ie. how many cid values are in the list.
    row.length_literal_row_candidate_cids = len(literal_row_candidate_cids)
    row.length_literal_row_max_cids = len(literal_row_max_cids)

    # Save the next id and previous id information into the row object.
    # This is used in the compare_chemicals_iterate_rows.html file.
    row.id_next = row_num + 1
    row.id_prev = row_num - 1

    # This step and getting 'id_next' and 'id_prev' is needed to correctly reflect
    # the url row information to match what is displayed on the page - the index is
    # not 0 based and hence this is necessary.
    if row.id_next > length_tanimoto:
        row.id_next = 1
    if row.id_prev <= 0:
        row.id_prev = length_tanimoto

    # Attach the cids to the pubchem path which is then fed into the iframes in
    # compare_chemicals_iterate_rows.html
    row.first_candidate_cids_url = pubchem_path + str(literal_row_candidate_cids[0])
    row.first_max_cids_url = pubchem_path + str(literal_row_max_cids[0])
    return render(request, 'similar_compounds_tanimoto/compare_chemicals_iterate_rows.html', {'row': row})


def compare_chemicals_iterate_candidate_list(request, row_num, candidate_index, max_tanimoto_index):
    """
    Initiates the TanimotoUpdated object and retrieves the next cid value within the same row.
    'row_candidate_cids' and 'row_max_cids' has a list of cids (i.e row_candidate_cids: [123, 456, 789],
    max_cids: [321, 654, 987]). Clicking the 'row_candidate_cids' associated button from
    compare_chemicals_iterate_both_lists.html will retrieve the next cid value and the 'max_cids'
    associated button will behave in the same way to retrieve the next cid values from its list.
    Using those values, the cid values are combined with the pubchem path to display the url iframe
    results in compare_chemicals_iterate_both_lists.html.

    Parameters:
    -----------
    row_num : int : the current row number
    candidate_index : int : the index of the candidates list
    max_tanimoto_index : int : the index of the max tanimoto list
    -----------

    returns a 'row' dictionary
    """
    length_tanimoto = len(TanimotoUpdated.objects.all())

    pubchem_path = 'https://pubchem.ncbi.nlm.nih.gov/compound/'
    row = get_object_or_404(TanimotoUpdated, pk=row_num)

    row_candidate_cids = row.candidate_cids
    row_max_cids = row.max_cids

    # row_candidate_cids and row_max_cids are string values (ie "[123, 456]").
    # However, we want a list value (ie List[str]) - and hence we change them.
    literal_row_candidate_cids = literal_eval(row_candidate_cids)
    literal_row_max_cids = literal_eval(row_max_cids)

    # Get length of the lists -- ie. how many cid values are in the list.
    row.length_literal_row_candidate_cids = len(literal_row_candidate_cids)
    row.length_literal_row_max_cids = len(literal_row_max_cids)
    row.total_number_of_rows = length_tanimoto
    row.tanimoto_percent = str(round(float(row.max) * 100, 2)) + '%'

    row.candidate_index = candidate_index
    row.max_tanimoto_index = max_tanimoto_index

    # Save the next id and previous id information into the row object.
    row.id_next = row_num + 1
    row.id_prev = row_num - 1

    # This step and getting 'id_next' and 'id_prev' is needed to correctly reflect
    # the url row information to match what is displayed on the page - the index is
    # not 0 based and hence this is necessary.
    if row.id_next > length_tanimoto:
        row.id_next = 1
    if row.id_prev <= 0:
        row.id_prev = length_tanimoto

    # Button behaviors from the compare_chemicals_iterate_both_lists.html file to
    # retreieve the next/prev cids while maintaining the row information and the
    # current candidate_index/max_tanimoto_index so that the indices do not reset
    if request.POST.get('get_next_candidate_cid'):
        row.candidate_index += 1
        if row.candidate_index >= len(literal_row_candidate_cids):
            row.candidate_index = 0

    if request.POST.get('get_next_max_cid'):
        row.max_tanimoto_index += 1
        if row.max_tanimoto_index >= len(literal_row_max_cids):
            row.max_tanimoto_index = 0

    # The list is zero indexed. Therefore, without this code, the front page will
    # display (0 out of 4) for example. We want this to show (1 out of 4).
    row.display_purpose_candidate_index = row.candidate_index + 1
    row.display_purpose_max_tanimoto_index = row.max_tanimoto_index + 1

    # Append the cid values to the url to show to the iframes in the compare_chemicals_iterate_both_lists.html
    row.next_candidate_cids_url = pubchem_path + str(literal_row_candidate_cids[row.candidate_index])
    row.curr_max_cids_url = pubchem_path + str(literal_row_max_cids[row.max_tanimoto_index])

    return render(request, 'similar_compounds_tanimoto/compare_chemicals_iterate_both_lists.html', {'row': row})

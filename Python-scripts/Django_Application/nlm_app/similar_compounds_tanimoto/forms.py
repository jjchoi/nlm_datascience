from django import forms
import re
from .models import TanimotoUpdated


class JumpToRowForm(forms.Form):
    row_number = forms.CharField(widget=forms.TextInput(attrs={'style':'max-width: 5em'}))

    def clean(self):
        length_tanimoto = len(TanimotoUpdated.objects.all())

        clean_data = super().clean()
        row_num = clean_data.get('row_number')
        if not re.search('^[0-9]*$', row_num):
            raise forms.ValidationError("Row number should only contain numeric values.")

        elif int(row_num) > length_tanimoto:
            raise forms.ValidationError("There are a total of " + str(length_tanimoto) + " records. Please insert a valid row number.")

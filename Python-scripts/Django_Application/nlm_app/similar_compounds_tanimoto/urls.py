from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'), # ex: /
    path('compare_chemicals/', views.CompareChemicals.as_view(), name='compare_chemicals'), # ex: /compare_chemicals/
    path('compare_chemicals/row_num=<int:row_num>/', views.compare_chemicals_iterate_rows, name='compare_chemicals_iterate_rows'), # ex: /compare_chemicals_home/2/
    path('compare_chemicals/row_num=<int:row_num>/candidate_helper=<int:candidate_index>&max_tanimoto_helper=<int:max_tanimoto_index>', views.compare_chemicals_iterate_candidate_list, name='compare_chemicals_iterate_candidate_list'), # ex: /compare_chemicals_home/2/
]

from django.apps import AppConfig


class SimilarCompoundsTanimotoConfig(AppConfig):
    name = 'similar_compounds_tanimoto'

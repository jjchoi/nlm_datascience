from django.db import models

class Tanimoto(models.Model):
    candidates = models.TextField()
    max = models.TextField()
    hsdb_chemicals = models.TextField()
    max_hsdb_chemicals = models.TextField()
    max_cids = models.TextField()
    candidate_cids = models.TextField()

    def __str__(self):
        return f"candidate cid: {self.candidate_cids} and max cid: {self.max_cids}"


class TanimotoUpdated(models.Model):
    candidates = models.TextField()
    max = models.TextField()
    hsdb_chemicals = models.TextField()
    max_hsdb_chemicals = models.TextField()
    max_cids = models.TextField()
    candidate_cids = models.TextField()

    def __str__(self):
        return f"candidate cid: {self.candidate_cids} and max cid: {self.max_cids}"

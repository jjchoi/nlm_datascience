from django.contrib import admin
from .models import DissimilarTanimoto, DissimilarTanimotoWithScore
from import_export.admin import ImportExportModelAdmin

# Register your models here.
@admin.register(DissimilarTanimoto)
class ViewAdmin(ImportExportModelAdmin):
    pass

@admin.register(DissimilarTanimotoWithScore)
class ViewAdmin(ImportExportModelAdmin):
    pass

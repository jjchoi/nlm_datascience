from django.urls import path
from . import views


urlpatterns = [
    path('', views.home.as_view(), name='dissimilar_compounds_home'), # ex: /dissimilar_compounds/
    path('row_num=<int:row_num>/', views.compare_dissimilar_chemicals_iterate_rows, name='compare_dissimilar_chemicals_iterate_rows'), # ex: /dissimilar_compounds/row_num=2/
    path('row_num=<int:row_num>/candidate_helper=<int:candidate_index>&corresponding_cids_helper=<int:corresponding_cids_index>', views.compare_dissimilar_chemicals_iterate_both_lists, name='compare_dissimilar_chemicals_iterate_both_lists'), # ex: /compare_chemicals_home/2/candidate_helper=1&corresponding_cids_helper=1
]

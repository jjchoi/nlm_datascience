from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .models import DissimilarTanimotoWithScore
from ast import literal_eval
from dissimilar_compounds_tanimoto.forms import JumpToRowForm
from django.views.generic import TemplateView
from django.contrib import messages
import re


class home(TemplateView):
    template_name = 'dissimilar_compounds_tanimoto/dissimilar_tanimoto_home.html'

    # With any request that comes in, we are simply creating a blank form and rendering
    # it back out to the template
    def get(self, request):
        form = JumpToRowForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = JumpToRowForm(request.POST)
        if form.is_valid():
            text = form.cleaned_data['row_number']
            return HttpResponseRedirect('row_num=' + text)
        else:
            messages.error(request, form.errors.get_json_data()['__all__'][0]['message'])
        return redirect('dissimilar_compounds_home')


def compare_dissimilar_chemicals_iterate_rows(request, row_num):
    """ Initiates the DissimilarTanimotoWithScore object and retrieves the next or previous row's
    'row_candidates_cids' and 'row_corresponding_cids'. It takes the first values from each of
    the cids, appends those values to the pubchem url path, and is then fed into
    the row object that is then used by compare_dissimilar_chemicals_iterate_rows.html to
    display the information

    Parameters:
    -----------
    row_num: int : row number
    -----------

    returns a 'row' dictionary
    """
    length_tanimoto = len(DissimilarTanimotoWithScore.objects.all())

    pubchem_path = 'https://pubchem.ncbi.nlm.nih.gov/compound/'
    row = get_object_or_404(DissimilarTanimotoWithScore, pk=row_num)
    row_candidates_cids = row.candidates_cids
    row_corresponding_cids = row.corresponding_cids
    row_tanimoto_scores = row.tanimoto_scores
    row.total_number_of_rows = length_tanimoto

    literal_row_tanimoto_scores = literal_eval(row_tanimoto_scores)
    row.tanimoto_percent = str(round(float(literal_row_tanimoto_scores[0]) * 100, 2)) + '%'

    # row_candidates_cids and row_corresponding_cids are string values (ie "[123, 456]").
    # However, we want a list value (ie List[str]) - and hence we change them.
    literal_row_candidates_cids = literal_eval(row_candidates_cids)
    literal_row_corresponding_cids = literal_eval(row_corresponding_cids)

    # Get length of the lists -- ie. how many cid values are in the list.
    row.length_literal_row_candidates_cids = len(literal_row_candidates_cids)
    row.length_literal_row_corresponding_cids = len(literal_row_corresponding_cids)

    # Save the next id and previous id information into the row object.
    # This is used in the compare_chemicals_iterate_rows.html file.
    row.id_next = row_num + 1
    row.id_prev = row_num - 1

    # This step and getting 'id_next' and 'id_prev' is needed to correctly reflect
    # the url row information to match what is displayed on the page - the index is
    # not 0 based and hence this is necessary.
    if row.id_next > length_tanimoto:
        row.id_next = 1
    if row.id_prev <= 0:
        row.id_prev = length_tanimoto

    # Attach the cids to the pubchem path which is then fed into the iframes in
    # compare_chemicals_iterate_rows.html
    row.first_candidates_cids_url = pubchem_path + str(literal_row_candidates_cids[0])
    row.first_corresponding_cids_url = pubchem_path + str(literal_row_corresponding_cids[0])
    return render(request, 'dissimilar_compounds_tanimoto/compare_dissimilar_chemicals_iterate_rows.html', {'row': row})


def compare_dissimilar_chemicals_iterate_both_lists(request, row_num, candidate_index, corresponding_cids_index):
    """
    Initiates the DissimilarTanimotoWithScore object and retrieves the next cid value within the same row.
    'row_candidates_cids' and 'row_corresponding_cids' has a list of cids (i.e row_candidates_cids: [123, 456, 789],
    corresponding_cids: [321, 654, 987]). Clicking the 'row_candidates_cids' associated button from
    compare_chemicals_iterate_both_lists.html will retrieve the next cid value and the 'corresponding_cids'
    associated button will behave in the same way to retrieve the next cid values from its list.
    Using those values, the cid values are combined with the pubchem path to display the url iframe
    results in compare_dissimilar_chemicals_iterate_both_rows.html.

    Parameters:
    -----------
    row_num : int : the current row number
    candidate_index : int : the index of the candidates list
    corresponding_cids_index : int : the index of the corresponding cids list
    -----------

    returns a 'row' dictionary
    """
    length_tanimoto = len(DissimilarTanimotoWithScore.objects.all())

    pubchem_path = 'https://pubchem.ncbi.nlm.nih.gov/compound/'
    row = get_object_or_404(DissimilarTanimotoWithScore, pk=row_num)

    row_candidates_cids = row.candidates_cids
    row_corresponding_cids = row.corresponding_cids

    # row_candidates_cids and row_corresponding_cids are string values (ie "[123, 456]").
    # However, we want a list value (ie List[str]) - and hence we change them.
    literal_row_candidates_cids = literal_eval(row_candidates_cids)
    literal_row_corresponding_cids = literal_eval(row_corresponding_cids)

    # Get length of the lists -- ie. how many cid values are in the list.
    row.length_literal_row_candidates_cids = len(literal_row_candidates_cids)
    row.length_literal_row_corresponding_cids = len(literal_row_corresponding_cids)
    row.total_number_of_rows = length_tanimoto

    row_tanimoto_scores = row.tanimoto_scores
    literal_row_tanimoto_scores = literal_eval(row_tanimoto_scores)

    row.candidate_index = candidate_index
    row.corresponding_cids_index = corresponding_cids_index

    # Save the next id and previous id information into the row object.
    row.id_next = row_num + 1
    row.id_prev = row_num - 1

    # This step and getting 'id_next' and 'id_prev' is needed to correctly reflect
    # the url row information to match what is displayed on the page - the index is
    # not 0 based and hence this is necessary.
    if row.id_next > length_tanimoto:
        row.id_next = 1
    if row.id_prev <= 0:
        row.id_prev = length_tanimoto

    # Button behaviors from the compare_chemicals_iterate_both_lists.html file to
    # retreieve the next/prev cids while maintaining the row information and the
    # current candidate_index/corresponding_cids_index so that the indices do not reset
    if request.POST.get('get_next_candidate_cid'):
        row.candidate_index += 1
        if row.candidate_index >= len(literal_row_candidates_cids):
            row.candidate_index = 0

    if request.POST.get('get_corresponding_cid'):
        row.corresponding_cids_index += 1
        if row.corresponding_cids_index >= len(literal_row_corresponding_cids):
            row.corresponding_cids_index = 0

    # The list is zero indexed. Therefore, without this code, the front page will
    # display (0 out of 4) for example. We want this to show (1 out of 4).
    row.display_purpose_candidate_index = row.candidate_index + 1
    row.display_purpose_corresponding_cids_index = row.corresponding_cids_index + 1

    # Append the cid values to the url to show to the iframes in the compare_chemicals_iterate_both_lists.html
    row.next_candidates_cids_url = pubchem_path + str(literal_row_candidates_cids[row.candidate_index])
    row.curr_corresponding_cids_url = pubchem_path + str(literal_row_corresponding_cids[row.corresponding_cids_index])

    row.tanimoto_percent = str(round(float(literal_row_tanimoto_scores[row.corresponding_cids_index]) * 100, 2)) + '%'

    return render(request, 'dissimilar_compounds_tanimoto/compare_dissimilar_chemicals_iterate_both_lists.html', {'row': row})

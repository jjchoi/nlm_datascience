from django.apps import AppConfig


class DissimilarCompoundsTanimotoConfig(AppConfig):
    name = 'dissimilar_compounds_tanimoto'

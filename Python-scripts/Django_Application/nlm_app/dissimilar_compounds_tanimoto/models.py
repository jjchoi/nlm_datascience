from django.db import models

class DissimilarTanimoto(models.Model):
    candidates = models.TextField()
    corresponding_compounds = models.TextField()
    candidates_cids = models.TextField()
    corresponding_cids = models.TextField()
    #tanimoto_scores = models.TextField()

    def __str__(self):
        return f"candidate cid: {self.candidates_cids} and corresponding cids: {self.corresponding_cids}"


class DissimilarTanimotoWithScore(models.Model):
    candidates = models.TextField()
    corresponding_compounds = models.TextField()
    candidates_cids = models.TextField()
    corresponding_cids = models.TextField()
    tanimoto_scores = models.TextField()

    def __str__(self):
        return f"candidate cid: {self.candidates_cids} and corresponding cids: {self.corresponding_cids}"

from django.contrib import admin
from .models import BelowTanimotoThreshold
from import_export.admin import ImportExportModelAdmin

# Register your models here.
@admin.register(BelowTanimotoThreshold)
class ViewAdmin(ImportExportModelAdmin):
    pass

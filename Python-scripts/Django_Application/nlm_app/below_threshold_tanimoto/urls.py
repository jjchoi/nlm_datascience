from django.urls import path
from . import views


urlpatterns = [
    path('', views.home.as_view(), name='below_threshold_tanimoto'),
    path('row_num=<int:row_num>/', views.cid_iterate_rows, name='cid_iterate_rows'),
]

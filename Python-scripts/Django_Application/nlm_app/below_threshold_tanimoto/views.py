from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import TemplateView
from .models import BelowTanimotoThreshold
from below_threshold_tanimoto.forms import JumpToRowForm
from django.http import HttpResponseRedirect
from django.contrib import messages


class home(TemplateView):
    template_name = 'below_threshold_tanimoto/below_threshold_tanimoto.html'

    # With any request that comes in, we are simply creating a blank form and rendering
    # it back out to the template
    def get(self, request):
        form = JumpToRowForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = JumpToRowForm(request.POST)
        if form.is_valid():
            text = form.cleaned_data['row_number']
            return HttpResponseRedirect('row_num=' + text)
        else:
            messages.error(request, form.errors.get_json_data()['__all__'][0]['message'])
        return redirect('below_threshold_tanimoto')


def cid_iterate_rows(request, row_num):
    """ Initiates the BelowTanimotoThreshold object and gets cids. This function
    combines the cid information with the pubchem url path and is then ingested into
    the row object that is then used by below_threshold_tanimoto_iterate_rows.html to
    display the information

    Parameters:
    -----------
    row_num: int : row number
    -----------

    returns a 'row' dictionary
    """
    length_tanimoto = len(BelowTanimotoThreshold.objects.all())

    pubchem_path = 'https://pubchem.ncbi.nlm.nih.gov/compound/'
    row = get_object_or_404(BelowTanimotoThreshold, pk=row_num)
    row_cid = row.cids

    row.total_number_of_rows = length_tanimoto

    # Save the next id and previous id information into the row object.
    # This is used in the compare_chemicals_iterate_rows.html file.
    row.id_next = row_num + 1
    row.id_prev = row_num - 1

    # This step and getting 'id_next' and 'id_prev' is needed to correctly reflect
    # the url row information to match what is displayed on the page - the index is
    # not 0 based and hence this is necessary.
    if row.id_next > length_tanimoto:
        row.id_next = 1
    if row.id_prev <= 0:
        row.id_prev = length_tanimoto

    # Attach the cids to the pubchem path which is then fed into the iframes in
    # compare_chemicals_iterate_rows.html
    row.cid_url = pubchem_path + str(row_cid)

    return render(request, 'below_threshold_tanimoto/below_threshold_tanimoto_iterate_rows.html', {'row': row})

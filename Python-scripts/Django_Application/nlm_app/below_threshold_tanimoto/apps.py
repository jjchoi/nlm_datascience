from django.apps import AppConfig


class BelowThresholdTanimotoConfig(AppConfig):
    name = 'below_threshold_tanimoto'

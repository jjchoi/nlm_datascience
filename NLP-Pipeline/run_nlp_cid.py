# -*- coding: utf-8 -*-
"""
@author: ICF

This is the main script that integrates the text classification, summarization, and NER-based extraction function.

Please call from the command line with the PubChem CID of the chemical to be curated as argument. Only CIDs in the candidates list will work because the script uses
a precomputed table of CID-PMID matches.
"""
import os
ROOT = os.path.abspath(os.path.dirname(__file__))
import argparse
from Scripts import call_pubmed_pull_abstracts_cid 
from Scripts import call_text_class_function
from Scripts import call_text_sum_function
from Scripts import call_ner_function
import re
import sys
import pandas as pd
if __name__ == '__main__':

    try:
        parser = argparse.ArgumentParser()
        parser.add_argument("query", help="Enter a PubChem CID; only works for project candidate chemical CIDs",type=str) # define query argument and help
        args = parser.parse_args() # parse arguments
        print(args.query)
        pull_abstracts=call_pubmed_pull_abstracts_cid.pull_abstracts
        print(f"1/7 - Pulling abstracts for {args.query} . . . ")
        df=pull_abstracts(args.query) # invoke function to pull abstracts based on query and create dataframe
        if df.empty:
            print("No abstracts were found. Exiting program . . . ")
            sys.exit()
        print("2/7 - Applying text classifier . . . ")
        apply_text_classifier=call_text_class_function.apply_text_classifier
        print("3/7 - Prioritizing hazard-related abstracts . . . ")
        df_class=apply_text_classifier(df)   # invoke function to prioritize hazard-related abstracts based on dataframe of titles and abstracts
        text_sum=call_text_sum_function.text_sum
        print("4/7 - Summarizing hazard-related abstracts . . . ")
        summary=text_sum(df_class,nwords=250) # # invoke function to summarize hazard-related abstracts in specified number of words (250 used here)
        apply_ner_models=call_ner_function.apply_ner_models
        print("5/7 - Extracting PIO elements from hazard-related abstracts . . . ")
        df_ner=apply_ner_models(df_class) # invoke function to extract PIO elements from hazard-related abstracts
        print("6/7 - Writing outputs . . . ")
        ofn=re.sub(r'[^A-Za-z0-9 ]+', '', args.query) # output file directory based on query after stripping non-alpha characters
        path_output=os.path.abspath(os.path.join(os.path.dirname( __file__ ),  'Output',ofn)) # path to outputs
        if not os.path.exists(path_output):
            os.makedirs(path_output)
        path_ner=os.path.join(path_output,"PICO_Extractions_from_Hazard_Related_Abstracts.csv")
        if len(df_ner)>0:
            df_ner.to_csv(path_ner,index=False)
            path_ner_html=os.path.join(path_output,"PICO_Extractions_from_Hazard_Related_Abstracts.html")
            try:
                df_ner.to_html(path_ner_html,index=False,justify='left')
            except:
                pass
        else:
            print ("No PICO elements to extract")
        path_class=os.path.join(path_output,"Prioritized_Abstracts.csv")
        df_class.to_csv(path_class,index=False)
        path_class_html=os.path.join(path_output,"Prioritized_Abstracts.html")
        try:
            df_class.to_html(path_class_html,index=False,justify='left')
        except:
            pass
        if len(summary)>0:
        
            path_sum=os.path.join(path_output,"Summary_Hazard_Related_Abstracts.txt")
            text_file = open(path_sum, "w")
            n = text_file.write(summary)
            text_file.close()
            df_sum=pd.DataFrame()
            df_sum["Summary"]=[summary]
            path_sum_html=os.path.join(path_output,"Summary_Hazard_Related_Abstracts.html")
            try:
                df_sum.to_html(path_sum_html,index=False,justify='left')
            except:
                pass
        else:
            print ("No summary of hazard relevant text to output")
        print("7/7 - Done.")
    except:
        print("Something went wrong. Please check query or try again with quotes around chemical name. Exiting program . . . ")
        sys.exit()

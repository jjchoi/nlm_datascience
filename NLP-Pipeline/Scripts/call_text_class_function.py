# -*- coding: utf-8 -*-
"""
@author: ICF

This support script calls the saved NB model trained on hazard related abstracts \
to prioritize dataframe of abstracts retrieved from PubMed based on relevance to hazard
"""
import joblib
import os

path_models=os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Models'))
path_vectorizer=os.path.join(path_models, 'vectorizer_sparse.pkl')
path_selector=os.path.join(path_models, 'selector_sparse.pkl')
path_classifier=os.path.join(path_models, 'nb_fs_sparse.pkl')

def apply_text_classifier(df):
    text=df['TiAb'].tolist()

    vectorizer = joblib.load(path_vectorizer)
    selector = joblib.load(path_selector)
    clf=joblib.load(path_classifier)
    
    X_test = selector.transform(vectorizer.transform(text))
    df['Hazard_Relevant']=clf.predict(X_test)
    df['Probability_Relevant']=clf.predict_proba(X_test)[:,1]
    
    df=df.sort_values(by=['Probability_Relevant'],ascending=False)
    return(df)

# -*- coding: utf-8 -*-
"""
@author: ICF

This support script calls the gensim text summarization function to summarize
only the hazard related abstracts based on the prior text classification function.

Because the gensim function works off text strings, the bytestrings are first encoded as strings. Unusual encodings may render this script ineffective.
"""

from gensim.summarization import summarize
import pandas as pd
import os




def string_from_list(ls): 
    str_join = " " 
    return (str_join.join(ls)) 

def bytes_from_bytelist(ls): 
    b_join = b" " 
    return (b_join.join(ls)) 


def text_sum(df_class,nwords):
    df_rel=df_class.loc[df_class['Hazard_Relevant']==1]
    if len(df_rel)>0:
        text=df_rel['TiAb'].tolist()
        stext=[]
        for tx in text:
            try:
                stx=tx.decode(encoding="utf-8")                
            except:
                try:
                    stx=tx.decode(encoding="win-1252")
                except:
                    stx=""
            stext.append(stx)
        try:
           if len(stext)>1E5:
                stext=stext[:1E4]
           stext=string_from_list(stext)
           summary=summarize(stext, word_count=500)
        except:
           summary=""
    else:
        summary=""
    return(summary)
# -*- coding: utf-8 -*-
"""
@author: ICF

Pulls titles and abstracts based on query
"""

from Bio import Entrez
import pandas as pd


Entrez.email = 'xyz.nxyz@gmail.com'


# Function to pull PMIDs by search term
        
def search(query):
    Entrez.email = Entrez.email 
    handle = Entrez.esearch(db='pubmed', 
                            sort='relevance', 
                            retmax='100000',
                            retmode='xml', 
                            term=query)
    results = Entrez.read(handle)
    return results

# Function to return PubMed records by PMID

def fetch_details(id_list):
    ids = ','.join(id_list)
    Entrez.email = Entrez.email
    handle = Entrez.efetch(db='pubmed',
                           retmode='xml',
                           retmax='100000',
                           id=ids)
    results = Entrez.read(handle)
    return results


def pull_abstracts(query):

    a=search(query)
    if a['Count']=='0':
        df=pd.DataFrame()
    else:
        b=fetch_details(a['IdList'])
        papers=b
        
        
        tlist=[]
        mlist=[]
        ablist=[] # list of abstracts
        tiablist=[]
        pylist=[]
        
        # for loop to extract PMIDs, titles and abstracts from returned records
        
        for i, paper in enumerate(papers['PubmedArticle']):
            try:
                abtxt=paper['MedlineCitation']['Article']['Abstract']['AbstractText'][0]
                if  isinstance(abtxt,str):
                    abtxt=abtxt.encode('ascii','ignore')
                else:
                    abtxt=str(abtxt)
                ablist.append(abtxt)
                ttxt=paper[u'MedlineCitation'][u'Article'][u'ArticleTitle']
                try:
                    pyear=paper[u'MedlineCitation'][u'Article'][u'ArticleDate'][0]['Year']
                except:
                    pyear=b""
                pylist.append(pyear)
                if  isinstance(ttxt,str):
                    ttxt=ttxt.encode('ascii','ignore')
                else:
                    ttxt=str(ttxt)
                tlist.append(ttxt)
                mlist.append(str(paper['MedlineCitation']['PMID']))
                tiab=ttxt+b" "+abtxt
                tiablist.append(tiab)
                
            except:
                ttxt=paper[u'MedlineCitation'][u'Article'][u'ArticleTitle']
                try:
                    pyear=paper[u'MedlineCitation'][u'Article'][u'ArticleDate'][0]['Year']
                except:
                    pyear=b""  
                ablist.append("")        
                mlist.append(str(paper['MedlineCitation']['PMID']))
                tlist.append(ttxt)
                tiab=ttxt#+" "+abtxt
                tiablist.append(tiab)
                pylist.append(pyear)            
        
        
        
    
        df=pd.DataFrame()
        
        df["Citation"]=mlist
        df["Title"]=tlist
        df["Abstract"]=ablist    
        df["TiAb"]=tiablist
        df["PubYear"]=pylist    
    
    return (df)



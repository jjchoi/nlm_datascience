# -*- coding: utf-8 -*-
"""
@author: ICF

This support script calls the saved NER PICO CRF models to extract PICO-element containing sentences from
the titles and abstracts of only those abstracts previously classified as relevant to hazard.
"""


import joblib
import os
import pandas as pd
import joblib
import nltk
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
from nltk.tokenize import sent_tokenize
from nltk.tokenize import word_tokenize
from nltk import pos_tag
import re

path_models=os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Models'))
path_ner_P=os.path.join(path_models, 'crf_model_participants.pkl')
path_ner_I=os.path.join(path_models, 'crf_model_interventions.pkl')
path_ner_O=os.path.join(path_models, 'crf_model_outcomes.pkl')




#### Define sentence parser function

def split_sentences(text): # function splits text into sentences per rules below
    caps = "([A-Z])"
    prefixes = "(Mr|St|Mrs|Ms|Dr)[.]"
    suffixes = "(Inc|Ltd|Jr|Sr|Co)"
    starters = "(Mr|Mrs|Ms|Dr|He\s|She\s|It\s|They\s|Their\s|Our\s|We\s|But\s|However\s|That\s|This\s|Wherever)"
    acronyms = "([A-Z][.][A-Z][.](?:[A-Z][.])?)"
    websites = "[.](com|net|org|io|gov)"
    numbers="(0|1|2|3|4|5|6|7|8|9)[.]"
    refs="(et al|Toxicol|Pharmacol|J|Soc|appl|Appl|Clin|Abst|Biochem|Env|Environ|Physiol|Contam|Bull|econ)[.]"
    text=str(text)
    text = " " + text + "  "
    text = text.replace("\n"," ")
    text = text.replace("i.e.","ie ")
    text = re.sub(prefixes,"\\1<prd>",text)
    text = re.sub(numbers,"\\1<prd>",text)
    text = re.sub(refs,"\\1<prd>",text)
    text = re.sub(websites,"<prd>\\1",text)
    if "Ph.D" in text: text = text.replace("Ph.D.","Ph<prd>D<prd>")
    text = re.sub("\s" + caps + "[.] "," \\1<prd> ",text)
    text = re.sub(acronyms+" "+starters,"\\1<stop> \\2",text)
    text = re.sub(caps + "[.]" + caps + "[.]" + caps + "[.]","\\1<prd>\\2<prd>\\3<prd>",text)
    text = re.sub(caps + "[.]" + caps + "[.]","\\1<prd>\\2<prd>",text)
    text = re.sub(" "+suffixes+"[.] "+starters," \\1<stop> \\2",text)
    text = re.sub(" "+suffixes+"[.]"," \\1<prd>",text)
    text = re.sub(" " + caps + "[.]"," \\1<prd>",text)
    if "”" in text: text = text.replace(".”","”.")
    if "\"" in text: text = text.replace(".\"","\".")
    if "!" in text: text = text.replace("!\"","\"!")
    if "?" in text: text = text.replace("?\"","\"?")
    text = text.replace(".",".<stop>")
    text = text.replace("?","?<stop>")
    text = text.replace("!","!<stop>")
    text = text.replace("<prd>",".")
    sentences = text.split("<stop>")
    sentences = sentences[:-1]
    sentences = [s.strip() for s in sentences]
    return sentences


def word2features(sent, i):
    word = sent[i][0]
    postag = sent[i][1]

    features = {
        'bias': 1.0,
        'word.lower()': word.lower(),
        'word[-3:]': word[-3:],
        'word[-2:]': word[-2:],
        'word.isupper()': word.isupper(),
        'word.istitle()': word.istitle(),
        'word.isdigit()': word.isdigit(),
        'postag': postag,
        'postag[:2]': postag[:2],
    }
    if i > 0:
        word1 = sent[i-1][0]
        postag1 = sent[i-1][1]
        features.update({
            '-1:word.lower()': word1.lower(),
            '-1:word.istitle()': word1.istitle(),
            '-1:word.isupper()': word1.isupper(),
            '-1:postag': postag1,
            '-1:postag[:2]': postag1[:2],
        })
    else:
        features['BOS'] = True

    if i < len(sent)-1:
        word1 = sent[i+1][0]
        postag1 = sent[i+1][1]
        features.update({
            '+1:word.lower()': word1.lower(),
            '+1:word.istitle()': word1.istitle(),
            '+1:word.isupper()': word1.isupper(),
            '+1:postag': postag1,
            '+1:postag[:2]': postag1[:2],
        })
    else:
        features['EOS'] = True

    return features    


def sent2features(sent):
    return [word2features(sent, i) for i in range(len(sent))]

def process_text_apply_crf(text,crf_model):    
#    sent_list=sent_tokenize(text)
    sent_list=split_sentences(text) # split text into sentences
    sent_token_list=[word_tokenize(x) for x in sent_list] # split each sentence into tokens
    tokenized_text=[(pos_tag(x)) for x in sent_token_list]  # add pos to each token in sentence
    crf_text=[sent2features(x) for x in tokenized_text] # create crf features    
    crf_tags=crf_model.predict(crf_text) # predict tags
    sents=[] # initialize list of sentences with relevant content
    for sent_ind,sent in enumerate(tokenized_text): # loop over sentences
        sent_flag=0 
        words=[] # initialize list of words
        for word_ind, word in enumerate(sent):
            tup=(tokenized_text[sent_ind][word_ind][0],crf_tags[sent_ind][word_ind]) # tuple of token, tag
            if (crf_tags[sent_ind][word_ind]!="O") and (len(sent_token_list[sent_ind])>5): # if tag is not O and sentence is greater than 5 
                sent_flag=1 # flag
            words.append(tup)
        if sent_flag==1:
            sents.append(sent_list[sent_ind]) # append relevant sentence only
    if sents==[]: # replace empty list with blank
        sents=""

    return(sents)


def apply_ner_models(df_class):
    
    df_ner = df_class.loc[df_class['Hazard_Relevant'] == 1]

    crf_model = joblib.load(path_ner_P)
    prt = df_ner['TiAb'].apply(process_text_apply_crf, crf_model=crf_model).tolist()

    crf_model = joblib.load(path_ner_I)
    irt = df_ner['TiAb'].apply(process_text_apply_crf, crf_model=crf_model).tolist()

    crf_model = joblib.load(path_ner_O)
    ort = df_ner['TiAb'].apply(process_text_apply_crf, crf_model=crf_model).tolist()

    df_ner_df = pd.DataFrame({'Citation': df_ner.Citation.tolist(),
                                'Title_Abstract': df_ner.TiAb.tolist(),
                              'Participants-Related_Text': prt,
                              'Interventions-Related_Text': irt,
                              'Outcomes-Related_Text': ort
                              })
    return df_ner_df

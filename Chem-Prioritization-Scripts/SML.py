# -*- coding: utf-8 -*-
"""
@author: ICF

This script implements the Supervised Machine Learning Analysis
Six different machine learning models are applied for a fixed number of iteration (1000)
This script uses a partial candidate set having all predictor variables
This script compiles a training dataset by combining HSDB chemicals (positives, in hsdb.csv) with a set of candidate chemicals identified by ICF as negatives (candidates_negative.csv)
The machine predictions are generated on the remaining pool of candidate chemicals (candidates_unclassified.csv)
"""

import numpy as np
import pandas as pd
import base64
from numpy.random import seed
seed(1)
import time
from sklearn.svm import LinearSVC
from sklearn.naive_bayes import BernoulliNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics
from sklearn.model_selection import cross_val_score
from sklearn import linear_model
from sklearn.ensemble import RandomForestClassifier 
from sklearn import tree
from sklearn.preprocessing import MinMaxScaler
import os


def plot_hist(df_inp,colhd,ofp): # plots histogram and density plot. Input dataframe, column head, and output file path.
    import matplotlib.pyplot as plt
    import seaborn as sns    
    sns.distplot(df_inp[colhd], hist=True, kde=True, 
             bins=int(180/5), color = 'darkblue', 
             hist_kws={'edgecolor':'black'},
             kde_kws={'linewidth': 4})    
    plt.title('Candidate Chemicals Supervised Machine Learning Normalized Ensemble Score Density Plot -')
    plt.xlabel('SML_Normalized Ensemble Score')
    plt.ylabel('Density')
    ofn="SML_Normalized Ensemble Score_x_"+str(niter) 
    ofpn=ofp+r'\\'+ofn
    plt.savefig(ofpn)
    plt.show()
    plt.close()

def conv_b64_bin(a): # converts binary 64 notation to bits
    try:
        b=base64.b64decode(a)
        c="".join(["{:08b}".format(x) for x in b])
        return(c)
    except:
        return("")
        
def add_cf_cols(df): # add chemical binary representation by parsing fingerprint
    df['ChemFing_String']=df['PUBCHEM_CACTVS_SUBSKEYS'].apply(conv_b64_bin)
    df['ChemFing_List']=df['ChemFing_String'].apply(list)    
    cfcols=["CF_"+str(i) for i in list(range(0,920))] # hard coded 920 CF length 
    df_cf=pd.DataFrame(df.ChemFing_List.values.tolist(), index= df.index)
    df_cf=df_cf.astype('int')
    df[cfcols]=df_cf
    df=df.drop(['ChemFing_String','ChemFing_List'],axis=1)
    return (df)    



def create_df_train(ifp,ifn_hsdb,ifn_can_neg): # create training df 
    df_neg=pd.read_csv(ifp+r'\\'+ifn_can_neg, header=0) 
    df_neg['HSDB_Suit']=0
    df_neg=df_neg.dropna(axis=0, how='any') # keep only rows without nas
#    df_neg=df_neg.drop(['candidates_CAN_SMILES'],axis=1)
            
    df_pos=pd.read_csv(ifp+r'\\'+ifn_hsdb, header=0) # generate self index
    df_pos['HSDB_Suit']=1
    df_pos=df_pos.dropna(axis=0, how='any') # keep only rows without nas
#    df_pos=df_pos.drop(['hsdb_CAN_SMILES'],axis=1)
    df_pos_sample=df_pos.sample(len(df_neg),replace=False)


    df_train=df_pos_sample.append(df_neg)
    df_train=add_cf_cols(df_train)    
   
    return (df_train)

def create_df_uc(ifp,ifn_can_uc,score_name): # read training df, unclassified candidates df

    df_uc=pd.read_csv(ifp+r'\\'+ifn_can_uc, header=0)
    df_uc=df_uc.dropna(axis=0, how='any') # keep only rows without nas
#    df_uc=df_uc.drop(['candidates_CAN_SMILES'],axis=1)
    df_uc=add_cf_cols(df_uc)
    df_uc[score_name]=0    
    
    return (df_uc)


def create_X_Y_train_X_uc(df_train,df_uc,label_name,score_name,non_pred_cols): # create Y_train, X_train after dropping non predictors and scaling continuous data
    Y_train=df_train[label_name].astype(np.float)
    X_train=df_train.drop(non_pred_cols+[label_name],axis=1)
    xtcols=X_train.columns # columns in X train
    xt_contcols=[col for col in xtcols if not("CF" in col) or not("HAZ" in col)] # columns with continuous data
    scaler = MinMaxScaler()
    X_train[xt_contcols] = scaler.fit_transform(X_train[xt_contcols])
    X_uc=df_uc.drop(non_pred_cols+[score_name],axis=1)
    xucols=X_train.columns # columns in X_uc
    xu_contcols=[col for col in xucols if not("CF" in col) or not("HAZ" in col)] # columns with continuous data
    X_uc[xu_contcols] = scaler.transform(X_uc[xu_contcols])
    
    return (X_train,Y_train,X_uc)




def ensemble_fit_pred(X_train,Y_train,X_uc,df_uc,clflist):
    score=[]
    for clf in clflist:
        clf.fit(X_train, Y_train) # Note: target has previously been created based on the whole training dataset
        pred=clf.predict(X_uc)
        df_uc['Pred']=pred
        df_uc['SML_Cumulative_Score']=df_uc.fillna(0)['SML_Cumulative_Score']+ df_uc.fillna(0)['Pred']   
        df_uc=df_uc.drop(['Pred'], axis=1)
        f1=np.mean(cross_val_score(clf, X_train, Y_train, cv=5, scoring='f1_macro'))
        print ("F1 score for ", clf, " is: ", f1)
        if not(np.isnan(f1)):
            score.append(f1)        
        f1_mean=np.mean(score)    
    return (df_uc, f1_mean)

def normalize_col(df,colname): # normalizes a single column in pd dataframe and returns a list
    scaler = MinMaxScaler() 
    x=df[colname].values.astype(float)    # float array
    x=x.reshape(-1,1)
    x_scaled=scaler.fit_transform(x) 
    return (x_scaled)

    
def main(path, ifn_hsdb, ifn_can_neg, ifn_can_uc, niter):
    ifp=path+r'\Data' # input file path
    ofp=path+r'\Output\SML' # output file path
    if not os.path.isdir(ofp):
        os.makedirs(ofp)

    non_pred_cols=['PUBCHEM_OPENEYE_CAN_SMILES','PUBCHEM_COMPOUND_CID','PUBCHEM_IUPAC_NAME','PUBCHEM_IUPAC_INCHI','PUBCHEM_IUPAC_INCHIKEY','PUBCHEM_MOLECULAR_FORMULA','PUBCHEM_OPENEYE_ISO_SMILES','PUBCHEM_CACTVS_SUBSKEYS','UNII','BIOWIN3_Ultimate_Timeframe']
    label_name='HSDB_Suit'
    score_name='SML_Cumulative_Score'
    clflist=[BernoulliNB(alpha=.01),KNeighborsClassifier(),LinearSVC(dual=False),linear_model.LogisticRegression(solver='lbfgs',max_iter=10000),RandomForestClassifier(n_estimators=100),tree.DecisionTreeClassifier()]
#    niter=500

    df_uc=create_df_uc(ifp,ifn_can_uc,score_name)
    
    f1_cum=[]
    for i in range(0,niter):
        df_train=create_df_train(ifp,ifn_hsdb,ifn_can_neg)
        X_train,Y_train,X_uc=create_X_Y_train_X_uc(df_train,df_uc,label_name,score_name,non_pred_cols)    
        df_uc,f1_run=ensemble_fit_pred(X_train,Y_train,X_uc,df_uc,clflist)
        f1_cum.append(f1_run)
        
    df_uc['SML_Norm_Score']=normalize_col(df_uc,'SML_Cumulative_Score') #call function to normalize cumulative score 
    df_uc=df_uc.sort_values(by='SML_Norm_Score',ascending=False) # sort in descending order of relevance
    plot_hist(df_uc,'SML_Norm_Score',ofp) # plot histogram    
    df_uc.to_csv(ofp+r'\\Supervised_Machine_Learning_Priority_Candidates.csv') # output csv including cumulative scores.
    
if __name__ == "__main__": 
    root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    ifn_hsdb=r'hsdb.csv'
    ifn_can_neg=r'candidates_negative.csv'
    ifn_can_uc=r'candidates_unclassified.csv'
    niter=500
    main(root, ifn_hsdb, ifn_can_neg, ifn_can_uc, niter) 
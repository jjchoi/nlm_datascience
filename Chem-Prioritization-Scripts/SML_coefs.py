# -*- coding: utf-8 -*-
"""
@author: ICF

This script identifies the prinicpal predictors in the SML algorithm.
Specifically, only the random forest model was analyzed for convenience.
This script compiles a training dataset by combining HSDB chemicals (positives, in hsdb.csv) with a set of candidate chemicals identified by ICF as negatives (candidates_negative.csv)
The machine predictions are generated on the remaining pool of candidate chemicals (candidates_unclassified.csv)
"""

import os
import numpy as np
import pandas as pd
import base64
from numpy.random import seed
seed(1)
import time
from sklearn.svm import LinearSVC
from sklearn.naive_bayes import BernoulliNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn import linear_model
from sklearn.ensemble import RandomForestClassifier 
from sklearn import tree
from sklearn.preprocessing import MinMaxScaler


def conv_b64_bin(a): # converts binary 64 notation to bits
    try:
        b=base64.b64decode(a)
        c="".join(["{:08b}".format(x) for x in b])
        return(c)
    except:
        return("")
        
def add_cf_cols(df):
    df['ChemFing_String']=df['PUBCHEM_CACTVS_SUBSKEYS'].apply(conv_b64_bin)
    df['ChemFing_List']=df['ChemFing_String'].apply(list)    
    cfcols=["CF_"+str(i) for i in list(range(0,920))] # hard coded 920 CF length 
    df_cf=pd.DataFrame(df.ChemFing_List.values.tolist(), index= df.index)
    df_cf=df_cf.astype('int')
    df[cfcols]=df_cf
    df=df.drop(['ChemFing_String','ChemFing_List'],axis=1)
    return (df)    



def create_df_train(ifp,ifn_hsdb,ifn_can_neg): # read training df, unclassified candidates df
    df_neg=pd.read_csv(ifp+r'\\'+ifn_can_neg, header=0) # use available C prefix index
    df_neg['HSDB_Suit']=0
    df_neg=df_neg.dropna(axis=0, how='any') # keep only rows without nas
#    df_neg=df_neg.drop(['candidates_CAN_SMILES'],axis=1)
            
    df_pos=pd.read_csv(ifp+r'\\'+ifn_hsdb, header=0) # generate self index
    df_pos['HSDB_Suit']=1
    df_pos=df_pos.dropna(axis=0, how='any') # keep only rows without nas
#    df_pos=df_pos.drop(['hsdb_CAN_SMILES'],axis=1)
    df_pos_sample=df_pos.sample(len(df_neg),replace=False)


    df_train=df_pos_sample.append(df_neg)
    df_train=add_cf_cols(df_train)    
   
    return (df_train)


def create_df_uc(ifp,ifn_can_uc,score_name): # read training df, unclassified candidates df

    df_uc=pd.read_csv(ifp+r'\\'+ifn_can_uc, header=0)
    df_uc=df_uc.dropna(axis=0, how='any') # keep only rows without nas
#    df_uc=df_uc.drop(['candidates_CAN_SMILES'],axis=1)
    df_uc=add_cf_cols(df_uc)
    df_uc[score_name]=0    
    
    return (df_uc)


def create_X_Y_train_X_uc(df_train,df_uc,label_name,score_name,non_pred_cols): # create Y_train, X_train after dropping non predictors and scaling continuous data
    Y_train=df_train[label_name].astype(np.float)
    X_train=df_train.drop(non_pred_cols+[label_name],axis=1)
    xtcols=X_train.columns # columns in X train
    xt_contcols=[col for col in xtcols if not("CF" in col or"HAZ_" in col)] # columns with continuous data
    scaler = MinMaxScaler()
    X_train[xt_contcols] = scaler.fit_transform(X_train[xt_contcols])
    X_uc=df_uc.drop(non_pred_cols+[score_name],axis=1)
    xucols=X_uc.columns # columns in X_uc
    xu_contcols=[col for col in xucols if not("CF" in col or"HAZ_" in col)] # columns with continuous data
    X_uc[xu_contcols] = scaler.transform(X_uc[xu_contcols])
    
    return (X_train,Y_train,X_uc)




def fit_coef(X_train,Y_train,clf,n_preds):
        clf.fit(X_train, Y_train) # Note: target has previously been created based on the whole training dataset
#        cf=clf.coef_
        cf=clf.feature_importances_
#        cfmax=np.argsort(cf[0])[-n_preds:]
        cfmax=np.argsort(cf)[-n_preds:]
        cols=X_train.columns[cfmax]
        return(set(cols))  


def normalize_col(df,colname): # normalizes a single column in pd dataframe and returns a list
    scaler = MinMaxScaler() 
    x=df[colname].values.astype(float)    # float array
    x=x.reshape(-1,1)
    x_scaled=scaler.fit_transform(x) 
    return (x_scaled)


def main(ifp, ifn_hsdb, ifn_can_neg, ifn_can_uc, ofp, niter):
    
    non_pred_cols=['UNII','PUBCHEM_OPENEYE_CAN_SMILES','PUBCHEM_COMPOUND_CID','PUBCHEM_IUPAC_NAME','PUBCHEM_IUPAC_INCHI','PUBCHEM_IUPAC_INCHIKEY','PUBCHEM_MOLECULAR_FORMULA','PUBCHEM_OPENEYE_ISO_SMILES','PUBCHEM_CACTVS_SUBSKEYS','BIOWIN3_Ultimate_Timeframe']
    label_name='HSDB_Suit'
    score_name='SML_Cumulative_Score'

    ## Choose single classifier -- note, downstream syntax varies accordingly
    #clflist=[BernoulliNB(alpha=.01),KNeighborsClassifier(),LinearSVC(),linear_model.LogisticRegression(),RandomForestClassifier(),tree.DecisionTreeClassifier()]
    #clf=LinearSVC(max_iter=100000,dual=False)
    clf=RandomForestClassifier(n_estimators=100)
#    niter=1000 # number of iterations
    n_preds=10  # number of main predictors
    
    df_uc=create_df_uc(ifp,ifn_can_uc,score_name)
    
    main_preds=set() # create empty set of main predictors
    
    pred_list=[] # create empty list of predictor
    
    for i in range(0,niter):
        df_train=create_df_train(ifp,ifn_hsdb,ifn_can_neg)
        X_train,Y_train,X_uc=create_X_Y_train_X_uc(df_train,df_uc,label_name,score_name,non_pred_cols)    
        preds=fit_coef(X_train,Y_train,clf,n_preds) # pull top n predictors
        main_preds=main_preds.union(preds) # union with main_preds
        pred_list.append(list(preds)) # save iteration top pred as item in pred_list
    
    print (len(main_preds)) # how many main preds
    print (main_preds) # which main preds
    
    phychem_preds=[col for col in main_preds if not("CF" in col)] # pull only phychem preds
    
    print(len(phychem_preds)) # how many phy_chem preds in main_preds
    
    print(len(phychem_preds)/len(main_preds)) # what ratio phychem preds 
    
    pred_list_flat=[x for y in pred_list for x in y] # flatten list of each iteration main preds
    import collections, operator
    
    fdist=dict(collections.Counter(pred_list_flat)) # # dictionary of occurrence of each pred in pred list
    fdist_sort = sorted(fdist.items(),key = operator.itemgetter(1),reverse = True) # sorted descending
    
    fdist_df=pd.DataFrame(fdist_sort,columns=['Predictor','Frequency']) # dictionary of occurrence of each pred in pred list
    fdist_df['Frequency']=fdist_df['Frequency']/niter # frequency
    
    print (fdist_df)
    ofpn=ofp+r'\\RandomForest_MainPreds_FreqDist.csv'
    fdist_df.to_csv(ofpn,index=False)
    
    #plt.bar(fdist.keys(), fdist.values(), width=1, color='g')

if __name__ == "__main__":
    root=os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    ofp=root+r'\Output\SML-coefs'
    if not os.path.isdir(ofp):
        os.makedirs(ofp)

    ifp=root+r'\Data'
    ifn_hsdb=r'hsdb.csv'
    ifn_can_neg=r'candidates_negative.csv'
    ifn_can_uc=r'candidates_unclassified.csv'
    niter=1000
    main(ifp, ifn_hsdb, ifn_can_neg, ifn_can_uc, ofp, niter)
# -*- coding: utf-8 -*-
"""
@author: ICF

This script implements the Gower Similarity Analysis
It computes similarity matrix between each candidate chemical (candidates.csv) and each hsdb chemical (hsdb.csv)
It also computes summary metrics of average and maximum similarity
The script has a toggle switch called na_action, which alternates between either a complete
candidate set with a partial set of predictors or a partial candidate set having all predictor variables 
"""

import numpy as np
import pandas as pd
import gower
import base64
from numpy.random import seed,random_integers
seed(1)
import time
from pyclustering.cluster.kmedoids import kmedoids
import os

def conv_b64_bin(a): # converts binary 64 notation to bits
    try:
        b=base64.b64decode(a)
        c="".join(["{:08b}".format(x) for x in b])
        return(c)
    except:
        return("")


def load_data(ifp,ifn_hsdb,ifn_can,na_action): # load can and hsdb data, set index, and remove rows with any missing data

    df_hsdb=pd.read_csv(ifp+r'\\'+ifn_hsdb, header=0) # read original data as dataframe
    df_hsdb=df_hsdb.dropna(axis=0, how='any') # keep only rows without nas

    hsdb_ind=['H_'+str(x) for x in list(np.arange(len(df_hsdb)))] # create index list and apply to hsdb dataframe
    df_hsdb.index=hsdb_ind
    df_hsdb=df_hsdb[df_hsdb['PUBCHEM_COMPOUND_CID'].notnull()]
    
    
    df_can=pd.read_csv(ifp+r'\\'+ifn_can, header=0) # read original data as dataframe
    if na_action=="Rows_NA_Removed":
        df_can=df_can.dropna(axis=0, how='any') # keep only rows without nas

    can_ind=['C_'+str(x) for x in list(np.arange(len(df_can)))] # create index list and apply to can dataframe
    df_can.index=can_ind
    df_can=df_can[df_can['PUBCHEM_COMPOUND_CID'].notnull()]    
    

    return(df_can,df_hsdb)

def process_data(df_can,df_hsdb,na_action): # appends df_Can to df_hsdb. Expands chemical fingerprint. Drops non numierc columns. Drops any columns with missing data.

    df=df_can.append(df_hsdb)    
    df=df[df['PUBCHEM_COMPOUND_CID'].notnull()]  # drop missing pubchem
    
    df['ChemFing_String']=df['PUBCHEM_CACTVS_SUBSKEYS'].apply(conv_b64_bin)
    df['ChemFing_List']=df['ChemFing_String'].apply(list)    
    cfcols=["CF_"+str(i) for i in list(range(0,920))] # hard coded 920 CF length 
    df_cf=pd.DataFrame(df.ChemFing_List.values.tolist(), index= df.index)
    df_cf=df_cf.astype('int')
    df[cfcols]=df_cf
        
    num = [] # drop non numeric columns
    for col in df:
        if (pd.api.types.is_numeric_dtype(df[col])):
            num.append(col)           
    df_pred = df[num]
    if na_action=="Rows_NA_Removed": 
        df_pred=df_pred.dropna(axis=0, how='any') # drop any rows with missing data
    else:
        df_pred=df_pred.dropna(axis=1, how='any') # drop any columns with missing data
    
    return (df,df_pred)


def gower_matrix(df,df_pred): # computes gower distance based similarity matrix

    ind=df_pred.index # index of df_pred 
    X_pred=np.asarray(df_pred)  # as array
    X_gower=gower.gower_matrix(X_pred) # computed gower distance based similarity matrix
    X_gower = 1-X_gower # convert to similarity matrix
    df_gower=pd.DataFrame(X_gower, index=ind,columns=ind) # as datafrme with original index and index as columns also
    df_CxH=df_gower.filter(regex='C_', axis=0)  # Candidate rows only
    df_CxH=df_CxH.filter(regex='H_', axis=1)  # HSDB cols only      
    df_CxH['Max']=df_CxH.max(axis=1) # maximum over row
    maxi=df_CxH.idxmax(axis=1) # index of maximum over row
    df_CxH['Min']=df_CxH.min(axis=1) # minimum over row
    mini=df_CxH.idxmin(axis=1) # index of minimum over row
    df_CxH['Max_HSDB_Sim_Index']=maxi
    df_CxH['Max_HSDB_Sim_Name']=list(df.loc[maxi]['PUBCHEM_IUPAC_NAME'])    
    df_CxH['Min_HSDB_Sim_Index']=mini
    df_CxH['Min_HSDB_Sim_Name']=list(df.loc[mini]['PUBCHEM_IUPAC_NAME'])    
    df_CxH['Mean']=df_CxH.mean(axis=1) # mean over row    
    can_ind=df_CxH.index  # index of candidate chems
    df_CxH['Can_CID']=df.loc[can_ind]['PUBCHEM_COMPOUND_CID'] # look up CIDs for can chems and insert as column
    df_CxH['Can_IUPAC_NAME']=df.loc[can_ind]['PUBCHEM_IUPAC_NAME'] # look up CIDs for can chems and insert as column

    hsdb_ind=df_CxH.columns # indices of hsdb chems
    df_CxH.loc['HSDB_Lookup'] = list(df.loc[hsdb_ind]['PUBCHEM_COMPOUND_CID']) # look up CIDs for hsdb chems and insert as row
    df_CxH.loc['HSDB_IUPAC_NAME'] = list(df.loc[hsdb_ind]['PUBCHEM_IUPAC_NAME']) # look up CIDs for hsdb chems and insert as row
    
    return (X_gower,df_gower,df_CxH)

def plot_hist(df_inp,colhd,ofp,na_action): # plots histogram and density plot. Input dataframe, column head, and output file path.
    import matplotlib.pyplot as plt
    import seaborn as sns
    
    sns.distplot(df_inp[colhd], hist=True, kde=True, 
             bins=int(180/5), color = 'darkblue', 
             hist_kws={'edgecolor':'black'},
             kde_kws={'linewidth': 4})    
    plt.title('Candidate-to-HSDB Chemical Similarity Score Density Plot -' + colhd)
    plt.xlabel('Gower distance')
    plt.ylabel('Density')
    ofn="GD_Similarity_Density_"+na_action+"_"+colhd
    ofpn=ofp+r'\\'+ofn
    plt.savefig(ofpn)
    plt.show()
    plt.close()
    
def clusters_visualize(df_gower,X_gower,ncl,ofp,na_action): # runs kmediods clustering and visualizes clusters and target on separate plots using PCA

    from sklearn.decomposition import PCA
    import seaborn as sns
    import matplotlib.pyplot as plt

    pca = PCA(n_components=2).fit(df_gower) # perform pca 2D model fitting
    pca_2d = pca.transform(df_gower) # transform gower matrix data to 2D
    df_pca=pd.DataFrame(pca_2d,columns=['X','Y']) # convert to dataframe
    
    initial_medoids=list(random_integers(0,X_gower.shape[0],ncl))
    kmedoids_instance = kmedoids(X_gower, initial_medoids, data_type='distance_matrix')
    kmedoids_instance.process()
    clusters = kmedoids_instance.get_clusters()

    ind_map={}
    for i in range(0, len(X_gower)):
        ind_map[i]=df_gower.index[i]

    clhd='Clusters'
    
    df_gower[clhd]=np.nan    
    for i,cl in enumerate(clusters):
        for j in cl:
            df_gower.loc[ind_map[j],[clhd]]=i        

    df_pca['Cluster']=list(df_gower[clhd])  # add cluster labels to df_pca. pretty sure indexing is preserved but check. 
    

    ind_can=df_gower.filter(regex='C_', axis=0).index # indices of candidate data
    ind_hsdb=df_gower.filter(regex='H_', axis=0).index # indicecs of hsdb data
    df_gower['Origin']="" # introduce data origin column
    df_gower.loc[ind_can, ['Origin']] = "Candidate"
    df_gower.loc[ind_hsdb, ['Origin']] = "HSDB"
    df_pca['Origin']=list(df_gower['Origin'])   # add to df_pca
    df_pca['Origin.x.Cluster']=df_pca['Origin'] +' _ '+ df_pca['Cluster'].map(str) # create combined cluster-origin variable
    sns.pairplot(x_vars=["X"], y_vars=["Y"], data=df_pca, hue="Origin", size=5)
    plt.title('PCA Visualization of Chemicals by DataTable _ '+str(ncl) )
    ofn="PCA_DataTable_"+na_action+"_"+str(ncl)
    ofpn=ofp+r'\\'+ofn
    plt.savefig(ofpn)

    sns.pairplot(x_vars=["X"], y_vars=["Y"], data=df_pca, hue="Cluster", size=5)
    plt.title('PCA Visualization of Chemicals by Cluster_ '+str(ncl) )
    ofn="PCA_Cluster_"+na_action+"_"+str(ncl)
    ofpn=ofp+r'\\'+ofn
    plt.savefig(ofpn)

    sns.pairplot(x_vars=["X"], y_vars=["Y"], data=df_pca, hue="Origin.x.Cluster", size=5)
    plt.title('PCA Visualization of Chemicals by DataTable and Cluster_ '+str(ncl) )
    ofn="PCA_Cluster_Origin_"+na_action+"_"+str(ncl)
    ofpn=ofp+r'\\'+ofn
    plt.savefig(ofpn)

    return()
    
def main(path, ifn_hsdb, ifn_can, na_action):
    ifp=path+r'\Data'
    ofp=path+r'\Output\GSM'
    if not os.path.isdir(ofp):
        os.makedirs(ofp)

    ncl=5 # number of clusters for visualization
    
    start_time=time.time()
    df_can,df_hsdb=load_data(ifp,ifn_hsdb,ifn_can,na_action) # load data
    df,df_pred= process_data(df_can,df_hsdb,na_action) # process data
    X_gower,df_gower,df_CxH=gower_matrix(df,df_pred) # compute gower dissimilarity matrix
    plot_hist(df_CxH,'Mean',ofp,na_action) # plot histogram by mean candidate similarity to HSDB 
    plot_hist(df_CxH,'Max',ofp,na_action) # plot histogram by max candidate similarity to HSDB 
    plot_hist(df_CxH,'Min',ofp,na_action) # plot histogram by max candidate similarity to HSDB 
    clusters_visualize(df_gower,X_gower,ncl,ofp,na_action) # visualize two clusters
    df_CxH.to_csv(ofp+r'\GD_CxH_'+na_action+'.csv',index=True) # output similarity matrix
   
if __name__ == "__main__": 
    root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    ifn_hsdb=r'hsdb.csv'
    ifn_can=r'candidates.csv'
    na_action="Rows_NA_Removed" # TOGGLE choose either this option or the one below
    #na_action="Cols_NA_Removed"
    main(root, ifn_hsdb, ifn_can, na_action)
    
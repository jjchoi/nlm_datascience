# -*- coding: utf-8 -*-
"""
@author: ICF

This script implements the Tanimoto Similarity Analysis
It computes similarity matrix between each candidate chemical (candidates.csv) and each hsdb chemical (hsdb.csv)
Requires awkward installation of rdkit (not through pip or conda install)
See https://www.rdkit.org/docs/Install.html
"""
import concurrent.futures
#import ipdb #breaks multiprocessing/multithreading!
import pandas as pd
import numpy as np

from rdkit import Chem
from rdkit import DataStructs
#from tqdm import tqdm

import time
import os

def TanimotoComparison(row_df:list, col_df:list) -> pd.DataFrame:
    """
    Computes Tanimoto similarity scores for a pair of chemicals
    
    Parameters
    ----------
    row_df : list
        row of chemical objects.
    col_df : list
        column of chemical objects.
    
    Returns
    -------
    A len(row_df) x len(col_df) dataframe of Tanimoto comparison scores.
    """    
    
    assert isinstance(row_df, list)
    assert isinstance(col_df, list)
    assert np.nan not in row_df
    assert np.nan not in col_df
    
    rows = len(row_df)
    cols = len(col_df)
    Tani_scores_list = [[0]*cols for _ in range(rows)] #initial matrix to hold Tanimoto scores
    #Tani_scores_list = [[0]*cols]*rows #bad syntax!
    #ipdb.set_trace()
    with concurrent.futures.ThreadPoolExecutor() as executor: #multiprocessing/multithreading: ThreadPoolExecutor was faster than ProcessPoolExecutor 
        for r, row in enumerate(row_df):
            for c, col in enumerate(col_df):
                Tani_scores_list[r][c] = (executor.submit(DataStructs.FingerprintSimilarity,row,col)).result() #function uses Tanimoto by default
                
    return(pd.DataFrame(Tani_scores_list))        


def main(path, ifn_hsdb, ifn_candidates):
    ifp=path+r'\Data' # input file path
    ofp=path+r'\Output\Tanimoto' # output file path
    if not os.path.isdir(ofp):
        os.makedirs(ofp)
    
    start_time = time.time()
    
    #candidates
    candidate_chemicals_df = pd.read_csv(ifp+r"\\"+ifn_candidates,engine='python', header=0, encoding="utf-8")
    candidate_smiles = candidate_chemicals_df.PUBCHEM_OPENEYE_CAN_SMILES.tolist() #list of candidate smiles
    assert np.nan not in candidate_smiles #the candidate smiles shouldn't contain any nan
    candidate_smiles_Mol = [Chem.MolFromSmiles(smiles) for smiles in candidate_smiles] #convert candidate smiles to chemical objects
    
    candidates_not_none = [i for i,v in enumerate(candidate_smiles_Mol) if v != None] #list of indices of non-None objects
    candidate_smiles_Mol = [candidate_smiles_Mol[i] for i in candidates_not_none] #list of non-None objects
    candidate_smiles = [candidate_smiles[i] for i in candidates_not_none] #list of non-None smiles
    candidate_smiles_Mol_fp = [Chem.RDKFingerprint(Mol) for Mol in candidate_smiles_Mol] #list of candidate chemical fingerprints

    #hsdb
    hsdb_chemicals_df = pd.read_csv(ifp+r"\\"+ifn_hsdb,engine='python', header = 0, encoding="utf-8")
    hsdb_smiles = hsdb_chemicals_df.PUBCHEM_OPENEYE_CAN_SMILES.tolist()
    assert np.nan not in hsdb_smiles
    hsdb_smiles_Mol = [Chem.MolFromSmiles(smiles) for smiles in hsdb_smiles]

    hsdb_not_none = [i for i,v in enumerate(hsdb_smiles_Mol) if v != None]
    hsdb_smiles_Mol = [hsdb_smiles_Mol[i] for i in hsdb_not_none]
    hsdb_smiles = [hsdb_smiles[i] for i in hsdb_not_none]
    hsdb_smiles_Mol_fp = [Chem.RDKFingerprint(Mol) for Mol in hsdb_smiles_Mol]
    
#==    
    Score_matrix =TanimotoComparison(candidate_smiles_Mol_fp, hsdb_smiles_Mol_fp)
    Score_matrix.index = candidate_smiles
    Score_matrix.columns = hsdb_smiles
    Score_matrix.to_csv(ofp+r"\\"+"Tani_out.csv")
    
    end_time = time.time()
    duration= (end_time - start_time)/3600 #in hours
    print(duration)
    
if __name__ == "__main__":
    root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    ifn_hsdb=r'hsdb.csv'
    ifn_candidates=r'candidates.csv'
    main(root, ifn_hsdb, ifn_candidates)
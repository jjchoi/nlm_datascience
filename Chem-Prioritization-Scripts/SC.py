# -*- coding: utf-8 -*-
"""
@author: ICF

This script implements the Supervised Clustering Analysis
Clustering is based off the Gower Similarity Matrix
Multiple iterations for three cluster sizes and three recall thresholds are computed
The script has a toggle switch called na_action, which alternates between either a complete
candidate set (candidates.csv) with a partial set of predictors or a partial candidate set having all predictor variables
"""

import numpy as np
import pandas as pd
import gower
import base64
from numpy.random import seed,random_integers
seed(1)
import time
from pyclustering.cluster.kmedoids import kmedoids
from copy import deepcopy
import operator
from sklearn.preprocessing import MinMaxScaler
import os



def conv_b64_bin(a): # converts binary 64 notation to bits
    try:
        b=base64.b64decode(a)
        c="".join(["{:08b}".format(x) for x in b])
        return(c)
    except:
        return("")

def plot_hist(df_inp,colhd,ofp,niter,nclusters,rec_thresh,na_action): # plots histogram and density plot. Input dataframe, column head, and output file path.
    import matplotlib.pyplot as plt
    import seaborn as sns    
    sns.distplot(df_inp[colhd], hist=True, kde=True, 
             bins=int(180/5), color = 'darkblue', 
             hist_kws={'edgecolor':'black'},
             kde_kws={'linewidth': 4})    
    plt.title('Candidate-to-HSDB Chemical Supervised Clustering Score Density Plot -')
    plt.xlabel('SC_Score')
    plt.ylabel('Density')
    ofn="SC_Score_Density_"+str(niter)+'x'+str(len(nclusters))+'x'+str(len(rec_thresh))+r'_'+na_action 
    ofpn=ofp+r'\\'+ofn
    plt.savefig(ofpn)
    plt.show()
    plt.close()

def load_data(ifp,ifn_hsdb,ifn_can,na_action): # load can and hsdb data, set index, and remove data missing CIDs

    df_hsdb=pd.read_csv(ifp+r'\\'+ifn_hsdb, header=0) # read hsdb data as dataframe
    hsdb_ind=['H_'+str(x) for x in list(np.arange(len(df_hsdb)))] # create index list 
    df_hsdb.index=hsdb_ind # reset index
    df_hsdb=df_hsdb[df_hsdb['PUBCHEM_COMPOUND_CID'].notnull()] # drop observations with missing CIDs
    
    
    df_can=pd.read_csv(ifp+r'\\'+ifn_can, header=0) # read candidate data as dataframe
    if na_action=="Rows_NA_Removed":
         df_can=df_can.dropna(axis=0, how='any') # keep only rows without nas      
    
    can_ind=['C_'+str(x) for x in list(np.arange(len(df_can)))] # create index list
    df_can.index=can_ind # reset index
    df_can=df_can[df_can['PUBCHEM_COMPOUND_CID'].notnull()]    # drop observations with missing CIDs
    
    return(df_can,df_hsdb)

def sample_hsdb(df_can,df_hsdb,nseeds): # sample nseeds randomly from hsdb data, adds to can data, removes non numeric column, removes columns with any missing data
    df_h=df_hsdb.dropna(axis=0, how='any') # keep only rows without nas
    df_hsdb_samp=df_h.sample(nseeds,replace=False,random_state=45) # sample without replacement with fixed seed    
    df=df_can.append(df_hsdb_samp)  # add to candidate chemicals data  
    df=df[df['PUBCHEM_COMPOUND_CID'].notnull()]   # drop observations with missing CIDs
    df=df.drop(['PUBCHEM_COMPOUND_CID'], axis=1) # drop CID column
    
    df['ChemFing_String']=df['PUBCHEM_CACTVS_SUBSKEYS'].apply(conv_b64_bin) # convert chemical fingerprint to binary
    df['ChemFing_List']=df['ChemFing_String'].apply(list)    
    cfcols=["CF_"+str(i) for i in list(range(0,920))] # hard coded 920 CF length 
    df_cf=pd.DataFrame(df.ChemFing_List.values.tolist(), index= df.index) # dataframe of chemical fingerprints only with same index as df
    df_cf=df_cf.astype('int')
    df[cfcols]=df_cf # add back to combined dataframe with seeds
        
    num = [] # drop non numeric fields
    for col in df:
        if (pd.api.types.is_numeric_dtype(df[col])):
            num.append(col)           
    df_pred = df[num]
    
#    df_pred=df_pred.dropna(axis=0, how='any')
    df_pred=df_pred.dropna(axis=1, how='any') # drop columns with missing data
    
    return (df_pred)


def kmed_cl(df_pred,ncl): # performs k mediods supervised clustering

    X_pred=np.asarray(df_pred) # convert to array
    X_gower=gower.gower_matrix(X_pred) # compute Gower dissimilarity matrix
    
    ind_map={} # map the index
    for i in range(0, len(X_pred)):
        ind_map[i]=df_pred.index[i]
        
    
    initial_medoids=list(random_integers(0,X_gower.shape[0],ncl)) # initial medoids at random
    kmedoids_instance = kmedoids(X_gower, initial_medoids, data_type='distance_matrix') # define instance
    kmedoids_instance.process() # invoke
    clusters = kmedoids_instance.get_clusters()
#        medoids = kmedoids_instance.get_medoids()
    clhd1='Cluster'# column head in output 
    df_cl=df_pred
    
    df_cl[clhd1]=np.nan    # initialize cluster column with nans
    
    for i,cl in enumerate(clusters): # replace with assigned cluster by looping over list of assigned clusters
        for j in cl: # cl contains a list of row numbers in X_gower
            df_cl.loc[ind_map[j],[clhd1]]=i    # index corresponding to row number
    

    return(df_cl)



def super_cl(df_cl,recthresh): # supervised clustering function computes recall using seeds and performs recall threshold based classification

    
    seedcolhead='Seed'
    clhd1='Cluster'
#
#    df1=df_cl.loc[df_cl[seedcolhead]==1] # Dataframe with positive seeds only
    df1=df_cl.filter(regex='H_', axis=0)  # Dataframe with seeds only
    df1[seedcolhead]=1 # For crosstab convenience

    npre=pd.crosstab(df1[clhd1],df1[seedcolhead]) # crosstab (numbers) of clusters v/s actual label for seeds only    
    npre1=npre[1].to_dict() # save label 1 distribution of clusters as dictionary. used to predict precision. 
    
    rec=pd.crosstab(df1[clhd1],df1[seedcolhead], normalize='columns') # crosstab (proportions) of clusters v/s actual labels for seeds only
    rec1=rec[1].to_dict() # save label 1 distribution of clusters as dictionary. used to predict recall.
    
    
    rec1=(sorted(rec1.items(), key=operator.itemgetter(1),reverse=True)) # sort in order of maximum recall. tuples of cluster and recall.
    
    rec_top=[x[0] for x in rec1] # sorted list of topics with maximum recall
    top=[] # initialize list
    cumtop=[] # initialize list     
    
    for x in rec_top:
        top.append(x)
        cumtop.append(deepcopy(top)) # contains list of lists of cumulative topics by recall
  
# compute predicted recall
    
    rec_pred={} # dictionary for recall predictions
    
    for topcombo in cumtop: # loop over all topic combinations in cumtop
        seed1=[]
        for t in topcombo: # loop over all topics within each topic combination            
             seed1.append(npre1[t]) # add label 1s for that topic to list
        ones=float(sum(seed1)) # sum label 1s for the topic combination
        rec_pred[str(topcombo)]=ones/sum(npre1.values())  # compute recall for that topic combination  
    
    srt={k:v for (k,v) in rec_pred.items() if v>recthresh}    # strategies above recall threshold
    
    strat=min(srt,key=srt.get)  # strategy (list of cluster numbers) corresponding to strategy just above recall threshold
    strat=eval(strat)# destring strat    -- a  bit unsafe in general but okay in our use
    clhd2='SC_Binary_Score'      
    df_cl[clhd2]=0 # label for strategy 1 set to default 0
    df_cl.loc[df_cl[clhd1].isin(strat), clhd2] = 1 # label for strategy updated if document falls into one of the strategy clusters
    return(df_cl) # returns expanded data as array with cluster and cluster-based classification, and a list of headers


def join_res (df_scl, df_can_op): # joins in results of supervised clustering and create cumulative similarity score

    df_scl=df_scl.filter(regex='C_', axis=0)    # keep only candidate chemical rows
    df_p=df_scl['SC_Binary_Score']    # keep only binary score for SCL function
    df_can_op=df_can_op.join(df_p)    # add to output
    df_can_op['SC_Cumulative_Score']=df_can_op.fillna(0)['SC_Cumulative_Score']+ df_can_op.fillna(0)['SC_Binary_Score']  # add to cumulative score 
    df_can_op=df_can_op.drop(['SC_Binary_Score'], axis=1) # drop binary score 
   
    return(df_can_op)


def normalize_col(df,colname): # normalizes a single column in pd dataframe and returns a list
    scaler = MinMaxScaler() 
    x=df[colname].values.astype(float)    # float array
    x=x.reshape(-1,1)
    x_scaled=scaler.fit_transform(x) 
    return (x_scaled)

def run_simulation(niter,df_can,df_hsdb,nseeds,nclusters,rec_thresh):
    df_can_op=df_can # set output df as candidate chemical dataframe to start
    df_can_op['SC_Cumulative_Score']=0 # introduce column for cumulative score
    for n in range(0,niter): # loop over iterations
       try: 
           print ("Iteration = ",n)
           df_pred=sample_hsdb(df_can,df_hsdb,nseeds) # create dataframe for prediction
           for rt in rec_thresh: # loop over alternative recall thresholds specified in input list
               print ("Recall Threshold = ",rt) 
               for ncl in nclusters:# loop over alternative cluster sizes specified in input list
                   print ("Clusters = ",ncl)
                   df_cl=kmed_cl(df_pred,ncl) # run kmedoids clsutering
                   df_scl=super_cl(df_cl,rt) # call supervised clustering                 
                   df_can_op=join_res(df_scl,df_can_op)
       except:
           continue
    return (df_can_op)

def main(path, ifn_hsdb, ifn_can, na_action, niter):
    ifp=path+r'\Data' # input file path
    ofp=path+r'\Output\SC' # output file path
    if not os.path.isdir(ofp):
        os.makedirs(ofp)
    
    nclusters=[5,10,15] # alternative cluster sizes
    rec_thresh=[0.25,0.50,0.75] # alternative recall thresholds
    nseeds=500 # number of seeds
 
    start_time=time.time() 
    df_can,df_hsdb=load_data(ifp,ifn_hsdb,ifn_can,na_action) # load data
    df_can_op=run_simulation(niter,df_can,df_hsdb,nseeds,nclusters,rec_thresh) # run simulation for all iterations, recall thresholds and cluster sizes
    ofn=r'\\Supervised_Clustering_Output_'+str(niter)+'x'+str(len(nclusters))+'x'+str(len(rec_thresh))+r'_'+na_action+'.csv'
    df_can_op['SC_Norm_Score']=normalize_col(df_can_op,'SC_Cumulative_Score') #call function to normalize cumulative score 
    df_can_op=df_can_op.sort_values(by='SC_Norm_Score',ascending=False) # sort in descending order of relevance
    plot_hist(df_can_op,'SC_Norm_Score',ofp,niter,nclusters,rec_thresh,na_action) # plot histogram
    fin_time=time.time()
    print ("Analysis ran in ", (fin_time-start_time)/3600," hrs")
    df_can_op.to_csv(ofp+ofn) # output csv including cumulative scores.

if __name__ == "__main__": 
    root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    ifn_hsdb=r'hsdb.csv' # hsdb input file name
    ifn_can=r'candidates.csv' # candidate input file name
    na_action="Rows_NA_Removed"
    #na_action="Cols_NA_Removed"
    niter=5# number of iterations
    main(root, ifn_hsdb, ifn_can, na_action, niter)    